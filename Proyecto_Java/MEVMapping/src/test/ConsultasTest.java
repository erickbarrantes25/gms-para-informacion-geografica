/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.ResultSet;
import org.json.JSONArray;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ebgic
 */
public class ConsultasTest {
    

     /**
     * Test of consultarCapas method, of class Consultas.
     */
    @Test
    public void testConsultarCapas() throws Exception {
        System.out.println("consultarCapas");
        ConexionPostgre cp = new ConexionPostgre();
        String nombre = "Volcanoes";
        Consultas instance = new Consultas();
        JSONArray result = null;
        result = instance.consultarCapas(cp, nombre);
        assertNotNull(result);
        
    }

    /**
     * Test of registrarUsuario method, of class Consultas.
     */
    @Test
    public void testRegistrarUsuario() throws Exception {
        System.out.println("registrarUsuario");
        ConexionPostgre cp = new ConexionPostgre();
        String userName = "Pedrito";
        String email = "pedropedro@gmail.com";
        String pass = "pedrito12345";
        Consultas instance = new Consultas();
        boolean expResult = true;
        boolean result = instance.registrarUsuario(cp, userName, email, pass);
        assertEquals(expResult, result);
       }

    /**
     * Test of insertarCapa method, of class Consultas.
     */
    @Test
    public void testInsertarCapa() throws Exception {
        System.out.println("insertarCapa");
        ConexionPostgre cp = new ConexionPostgre();
        String layerName = "Panadería";
        Consultas instance = new Consultas();
        boolean expResult = true;
        boolean result = instance.insertarCapa(cp, layerName);
        assertEquals(expResult, result);
    }

    /**
     * Test of insertarField method, of class Consultas.
     */
    @Test
    public void testInsertarField() throws Exception {
        System.out.println("insertarField");
        ConexionPostgre cp = new ConexionPostgre();
        int layer = 5;
        String instr = "Nombre Panaderia";
        String type = "varchar()";
        boolean req = true;
        int multi = 1;
        Consultas instance = new Consultas();
        boolean expResult = true;
        boolean result = instance.insertarField(cp, layer, instr, type, req, multi);
        assertEquals(expResult, result);
    }

    /**
     * Test of actualizarField method, of class Consultas.
     */
    @Test
    public void testActualizarField() throws Exception {
        System.out.println("actualizarField");
        ConexionPostgre cp = new ConexionPostgre();
        int layer = 5;
        String instrOld = "Nombre Panaderia";
        String instrNew = "Nombre Panaderia";
        String type = "varchar()";
        boolean req = false;
        int multi = 7;
        Consultas instance = new Consultas();
        boolean expResult = true;
        boolean result = instance.actualizarField(cp, layer, instrOld, instrNew, type, req, multi);
        assertEquals(expResult, result);
    }

    /**
     * Test of eliminarField method, of class Consultas.
     */
    @Test
    public void testEliminarField() throws Exception {
        System.out.println("eliminarField");
        ConexionPostgre cp = new ConexionPostgre();
        String instr = "Nombre Panaderia";
        int layer = 5;
        Consultas instance = new Consultas();
        boolean expResult = true;
        boolean result = instance.eliminarField(cp, instr, layer);
        assertEquals(expResult, result);
    }

    /**
     * Test of actualizarCapa method, of class Consultas.
     */
    @Test
    public void testActualizarCapa() throws Exception {
        System.out.println("actualizarCapa");
        ConexionPostgre cp = new ConexionPostgre();
        String layerNameOld = "Panadería";
        String layerNameNew = "Panaderías en Grecia";
        Consultas instance = new Consultas();
        boolean expResult = true;
        boolean result = instance.actualizarCapa(cp, layerNameOld, layerNameNew);
        assertEquals(expResult, result);
    }

    /**
     * Test of eliminarCapa method, of class Consultas.
     */
    @Test
    public void testEliminarCapa() throws Exception {
        System.out.println("eliminarCapa");
        ConexionPostgre cp = new ConexionPostgre();
        String layerName = "Panaderías en Grecia";
        Consultas instance = new Consultas();
        boolean expResult = true;
        boolean result = instance.eliminarCapa(cp, layerName);
        assertEquals(expResult, result);
    }

    /**
     * Test of main method, of class Consultas.
     */    
}
