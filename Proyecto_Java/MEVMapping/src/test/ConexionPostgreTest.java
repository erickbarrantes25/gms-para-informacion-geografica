/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.Connection;
import java.sql.ResultSet;
import org.json.JSONArray;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ebgic
 */
public class ConexionPostgreTest {

    /**
     * Test of iniciarConexion method, of class ConexionPostgre.
     */
    @Test
    public void testIniciarConexion() {
        System.out.println("iniciarConexion");
        ConexionPostgre instance = new ConexionPostgre();
        instance.iniciarConexion();
        assertNotNull(instance.getConnection());
    }

    /**
     * Test of getConnection method, of class ConexionPostgre.
     */
    @Test
    public void testGetConnection() {
        System.out.println("getConnection");
        ConexionPostgre instance = new ConexionPostgre();
        Connection result = instance.getConnection();
        assertNotNull(result);
    }

    /**
     * Test of finalizarConexion method, of class ConexionPostgre.
     */
    @Test
    public void testFinalizarConexion() {
        System.out.println("finalizarConexion");
        ConexionPostgre instance = new ConexionPostgre();
        instance.iniciarConexion();
        Connection result = instance.finalizarConexion();
        assertNull(result);
        
    }
    
}
