/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.ResultSet;
import java.util.ArrayList;
import org.json.JSONArray;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ebgic
 */
public class ConsultasTest2 {

   
    /**
     * Test of confirmarUsuario method, of class Consultas.
     */
    @Test
    public void testConfirmarUsuario() {
        System.out.println("confirmarUsuario");
        ConexionPostgre cp = new ConexionPostgre();
        String userName = "juanito1";
        String pass = "juan12345";
        Consultas instance = new Consultas();
        boolean expResult = true;
        boolean result = instance.confirmarUsuario(cp, userName, pass);
        assertEquals(expResult, result);
    }

    /**
     * Test of insertarFeature method, of class Consultas.
     */
    @Test
    public void testInsertarFeature() throws Exception {
        System.out.println("insertarFeature");
        ConexionPostgre cp = new ConexionPostgre();
        int layer = 1;
        Consultas instance = new Consultas();
        Input a = new Input();
        Input b = new Input();
        Input c = new Input();
        a.setName("Hospital de Grecia");
        a.setType("Nombre");
        b.setName("200");
        b.setType("Capacidad");
        c.setName("true");
        c.setType("Parqueo");
        ArrayList<Input> array = new ArrayList<>();
        array.add(a);
        array.add(b);
        array.add(c);
        String title = "Grecia";
        int user = 1;
        boolean expResult = true;
        boolean result = instance.insertarFeature(cp, layer, array, title, user);
        assertEquals(expResult, result);
    }

    /**
     * Test of consultarFeature method, of class Consultas.
     */
    @Test
    public void testConsultarFeature() {
        System.out.println("consultarFeature");
        ConexionPostgre cp = new ConexionPostgre();
        int idFeature = 1;
        int layerID = 1;
        Consultas instance = new Consultas();
        ArrayList<Input> result = instance.consultarFeature(cp, idFeature, layerID);
        assertNotNull(result);
    }
 
}
