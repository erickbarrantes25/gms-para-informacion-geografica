package com.mycompany.wevmapping.rest;

import com.mycompany.wevmapping.entities.Feature;
import com.mycompany.wevmapping.entities.Field;
import com.mycompany.wevmapping.entities.Input;
import com.mycompany.wevmapping.entities.Layer;
import com.mycompany.wevmapping.entities.Marker;
import com.mycompany.wevmapping.entities.Person;
import com.mycompany.wevmapping.gestores.ConexionPostgre;
import com.mycompany.wevmapping.gestores.Consultas;

import java.sql.SQLException;

//import com.mycompany.basesproyecto.gestores.JDBConnection;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;


@Path("/")
public class JsonService {
     
    
    
    //Lista
    @GET
    @Path("/listLayerFields/{layer}")
    @Produces({"application/json"})
    public static ArrayList<Field> consultarListaFieldsLayerInJSON(@PathParam("layer") String layer) throws ClassNotFoundException, SQLException, JSONException, org.json.JSONException {   
            ConexionPostgre c = new ConexionPostgre();
            Consultas con = new Consultas();
            //System.out.println(con.listaFieldsLayer(c, layer));
            ArrayList<Field> response = con.listaFieldsLayer(c, layer);
            c.finalizarConexion();
            
            return response;
    }
    /*
    //Lista
    @GET
    @Path("/listLayer")
    @Produces({"application/json"})
    public static ArrayList<Layer> consultarCapasInJSON() throws ClassNotFoundException, SQLException, JSONException, org.json.JSONException {   
            ConexionPostgre c = new ConexionPostgre();
            Consultas con = new Consultas();
            return con.consultarCapas(c);
    }
    */
    
    @GET
    @Path("/listLayer")
    @Produces({"application/json"})
    public static ArrayList<Layer> consultarCapasInJSON() throws ClassNotFoundException, SQLException, JSONException, org.json.JSONException {   
            ConexionPostgre c = new ConexionPostgre();
            Consultas con = new Consultas();
            ArrayList<Layer> arreglo = new ArrayList<>();
            ArrayList<Layer> todos = new ArrayList<>();
            todos = con.consultarCapas(c);
            for(int i = 0 ; i < todos.size();i++) {
                Layer nuevo = new Layer();
                nuevo = todos.get(i);
                nuevo.setFieldArray(con.listaFieldsLayer(c, nuevo.getlayername()));
                arreglo.add(nuevo);
            } 
            c.finalizarConexion();
            return arreglo; 
    }
    
    @GET
    @Path("/field/{id}")
    @Produces({"application/json"})
    public static ArrayList<Field> consultarFieldInJSON(@PathParam("id") int fieldid) throws ClassNotFoundException, SQLException, JSONException, org.json.JSONException {   
            ConexionPostgre c = new ConexionPostgre();
            Consultas con = new Consultas();
            Field field = new Field();
            ArrayList<Field> response =con.consultarField(c,fieldid);
            c.finalizarConexion();
            return response; 
    }
    
    //Lista
    @GET
    @Path("/listFeatureInput")
    @Produces({"application/json"})
    public static ArrayList<Feature> consultarListaFeatureInputInJSON() throws ClassNotFoundException, SQLException, JSONException, org.json.JSONException {   
            ConexionPostgre c = new ConexionPostgre();
            Consultas con = new Consultas();
            //System.out.println(con.listaFieldsLayer(c, layer));
            ArrayList<Feature> response = con.consultarTodosFeature(c);
            c.finalizarConexion();
            return response;
    }
    
    
    
    

    @GET
    @Path("/listFeature")
    @Produces({"application/json"})
    public static ArrayList<String> consultarListaFeatureInJSON() throws ClassNotFoundException, SQLException, JSONException, org.json.JSONException {   
            ConexionPostgre c = new ConexionPostgre();
            Consultas con = new Consultas();
            //System.out.println(con.listaFieldsLayer(c, layer));
            ArrayList<String> response = con.consultarFeature(c);
            c.finalizarConexion();
            return response;
            
    }
    @GET
    @Path("/login/userinfo/{id}")
    @Produces({"application/json"})
    public static Person consultarUserInfoInJSON(@PathParam("id") int userid) throws ClassNotFoundException, SQLException, JSONException, org.json.JSONException {   
            ConexionPostgre c = new ConexionPostgre();
            Consultas con = new Consultas();
            //System.out.println(con.listaFieldsLayer(c, layer));
            Person response = con.consultarUserInfo(c,userid);
            c.finalizarConexion();
            return response;
            
    }
    //Lista
    @POST
    @Path("/login")
    @Produces(MediaType.APPLICATION_JSON)
    public Response confirmarUsuarioInJSON(Person person) throws ClassNotFoundException, SQLException {
            ConexionPostgre c = new ConexionPostgre();
            Consultas con = new Consultas();
            String result = "No se pudo iniciar sesion";
            boolean[] response= con.confirmarUsuario(c, person.getName(), person.getPassword());
            if(response[0]){
                if(response[1]){
                    result = "Manager";
                
                }else result = "Normal";
                
            }
            c.finalizarConexion();
            return Response.status(200).entity(result).build();
    }
    
    @POST
    @Path("/login/userid")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsuarioInJSON(Person person) throws ClassNotFoundException, SQLException {
            ConexionPostgre c = new ConexionPostgre();
            Consultas con = new Consultas();
            int result = 0;
            int response= con.getUsuario(c, person.getName(), person.getPassword());
            c.finalizarConexion();
            System.out.println(result);
            return Response.status(200).entity(response).build();
    }
    
  
    @POST
    @Path("/map/markers")
    @Produces({"application/json"})
    public static ArrayList<Marker> consultarMarkersInJSON(int layerList[]) throws ClassNotFoundException, SQLException, JSONException, org.json.JSONException {   
            ConexionPostgre c = new ConexionPostgre();
            Consultas con = new Consultas();
            ArrayList<Marker> response =con.consultarMarker(c,layerList); 
            c.finalizarConexion();
            return response; 
    }
    
        
    @POST
    @Path("/add/feature/{layer}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addFeatureInJSON(@PathParam("layer") int layer, Feature feature) throws ClassNotFoundException, SQLException {
            ConexionPostgre c = new ConexionPostgre();
            Consultas con = new Consultas();
            String result = "No se pudo agregar el feature";
            if(con.insertarFeature(c, layer, feature.getArregloInput(), feature.getTitle(), feature.getUserID())){
                result = "Feature registrado correctamente";
            }
            c.finalizarConexion();
            return Response.status(200).entity(result).build();
    }
   
    //Lista
    @POST
    @Path("/add/user")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addPersonInJSON(Person person) throws ClassNotFoundException, SQLException {
            ConexionPostgre c = new ConexionPostgre();
            Consultas con = new Consultas();
            String result = "No se pudo agregar el usuario";
            if(con.registrarUsuario(c, person.getName(), person.getEmail(), person.getPassword())){
                result = "Usuario registrado correctamente";
                }
            c.finalizarConexion();
            return Response.status(200).entity(result).build();
    }
    
    
    //Lista
    @POST
    @Path("/add/layer/{layer}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addLayerInJSON(@PathParam("layer") String layer, ArrayList<Field> array) throws ClassNotFoundException, SQLException {
            String result = "No se pudo agregar";
            ConexionPostgre c = new ConexionPostgre();
            Consultas con = new Consultas();
            if(con.insertarCapa(c, layer)){
                result = "Layer agregado";
                addFieldInJSON(array, layer);
            }
            c.finalizarConexion();
            return Response.status(200).entity(result).build();
    }
    
    
    //Lista
    public boolean addFieldInJSON(ArrayList<Field> array, String layer) throws ClassNotFoundException, SQLException {
            ConexionPostgre c = new ConexionPostgre();
            Consultas con = new Consultas();
            String result = "No se pudo agregar los fields";
            if(con.insertarField(c, layer, array)){
                result = "Fields registrados correctamente";
                return true;
            }
            c.finalizarConexion();
            return false;
    }
    
    //Lista
    @POST
    @Path("/update/field/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateFieldInJSON(@PathParam("id") int id, Field field) throws ClassNotFoundException, SQLException {
            ConexionPostgre c = new ConexionPostgre();
            Consultas con = new Consultas();
            String result = "No se pudo actualizar";

            if(con.actualizarField(c, id, field)){
                result = "Field actualizado";
            }
            c.finalizarConexion();
            return Response.status(200).entity(result).build();
    }
    
    //Lista
    @DELETE
    @Path("/delete/field/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteFieldInJSON(@PathParam("id") int id) throws ClassNotFoundException, SQLException {
            ConexionPostgre c = new ConexionPostgre();
            Consultas con = new Consultas();
            String result = "No se encontro el field";
            if(con.eliminarField(c, id)){
                result = "Borrado Completo";
            }
            c.finalizarConexion();
            return Response.status(200).entity(result).build();
    }
    
    //Lista
    @POST
    @Path("/update/layer/")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateLayerInJSON(@PathParam("oldLayerName") String oldLayerName, Layer newl) throws ClassNotFoundException, SQLException {
            ConexionPostgre c = new ConexionPostgre();
            Consultas con = new Consultas();
            String result = "No se pudo actualizar";
       
            if(con.actualizarCapa(c, newl.getlayerid(), newl.getlayername())){
                result = "Layer actualizado";
            }
            c.finalizarConexion();
            return Response.status(200).entity(result).build();
    }
    
    @POST
    @Path("/update/addNewFields/{layername}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addNewFieldsInJSON(@PathParam("layername") String layerName,ArrayList<Field> array ) throws ClassNotFoundException, SQLException {
            ConexionPostgre c = new ConexionPostgre();
            Consultas con = new Consultas();
            String result = "No se pudo actualizar";
       
            if(addFieldInJSON(array, layerName)){
                result = "Layer actualizado";
      
            }
            c.finalizarConexion();
            return Response.status(200).entity(result).build();
    }
    
    //Listo
    @DELETE
    @Path("/delete/layer/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteCapaInJSON(@PathParam("id") int id) throws ClassNotFoundException, SQLException {
            ConexionPostgre c = new ConexionPostgre();
            Consultas con = new Consultas();
       
            String result = "No se encontro la tarea";
            if(con.eliminarCapa(c, id)){
                result = "Borrado Completo";
            }
            c.finalizarConexion();
            return Response.status(200).entity(result).build();
    }
    
    //Lista
    @POST
    @Path("/update/user")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateUserInJSON(Person person) throws ClassNotFoundException, SQLException {
            ConexionPostgre c = new ConexionPostgre();
            Consultas con = new Consultas();
            String result = "No se pudo actualizar";
       
            if(con.actualizarUsuario(c, person.getName(), person.getEmail(), person.getPassword(), person.getPersonID())){
                result = "Usuario actualizado";
            }
            c.finalizarConexion();
            return Response.status(200).entity(result).build();
    }

    
    @DELETE
    @Path("/delete/feature/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteFeatureInJSON(@PathParam("id") int id) throws ClassNotFoundException, SQLException {
            ConexionPostgre c = new ConexionPostgre();
            Consultas con = new Consultas();
            String result = "No se encontro el feature";
            if(con.eliminarFeature(c, id)){
                result = "Borrado Completo";
            }
            c.finalizarConexion();
            return Response.status(200).entity(result).build();
    }

    
    @POST
    @Path("/update/feature/{feature}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateFeaturerInJSON(@PathParam("feature") String feat, Feature feature) throws ClassNotFoundException, SQLException {
            ConexionPostgre c = new ConexionPostgre();
            Consultas con = new Consultas();
            String result = "No se pudo actualizar";
       
            if(con.actualizarFeature(c, feat, feature.getTitle())){
                result = "Feature actualizado";
            }
            c.finalizarConexion();
            return Response.status(200).entity(result).build();
    }
    
    @POST
    @Path("/update/input/{feature}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateInputInJSON(@PathParam("feature") String feature, Input input) throws ClassNotFoundException, SQLException {
            ConexionPostgre c = new ConexionPostgre();
            Consultas con = new Consultas();
            String result = "No se pudo actualizar";
       
            if(con.actualizarInput(c, feature, input.getName(), input.getType())){
                result = "Input actualizado";
            }
            c.finalizarConexion();
            return Response.status(200).entity(result).build();
    }
   
}
