/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wevmapping.gestores;

import com.mycompany.wevmapping.entities.*;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;
import org.postgresql.Driver;

 
import java.sql.ResultSetMetaData;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
;

public class ConexionPostgre {

    public static JSONArray convert(ResultSet rs)
            throws SQLException, JSONException {
        JSONArray json = new JSONArray();
        ResultSetMetaData rsmd = rs.getMetaData();

        while (rs.next()) {
            int numColumns = rsmd.getColumnCount();
            JSONObject obj = new JSONObject();

            for (int i = 1; i < numColumns + 1; i++) {
                String column_name = rsmd.getColumnName(i);

                if (rsmd.getColumnType(i) == java.sql.Types.ARRAY) {
                    obj.put(column_name, rs.getArray(column_name));
                } else if (rsmd.getColumnType(i) == java.sql.Types.BIGINT) {
                    obj.put(column_name, rs.getInt(column_name));
                } else if (rsmd.getColumnType(i) == java.sql.Types.BOOLEAN) {
                    obj.put(column_name, rs.getBoolean(column_name));
                } else if (rsmd.getColumnType(i) == java.sql.Types.BLOB) {
                    obj.put(column_name, rs.getBlob(column_name));
                } else if (rsmd.getColumnType(i) == java.sql.Types.DOUBLE) {
                    obj.put(column_name, rs.getDouble(column_name));
                } else if (rsmd.getColumnType(i) == java.sql.Types.FLOAT) {
                    obj.put(column_name, rs.getFloat(column_name));
                } else if (rsmd.getColumnType(i) == java.sql.Types.INTEGER) {
                    obj.put(column_name, rs.getInt(column_name));
                } else if (rsmd.getColumnType(i) == java.sql.Types.NVARCHAR) {
                    obj.put(column_name, rs.getNString(column_name));
                } else if (rsmd.getColumnType(i) == java.sql.Types.VARCHAR) {
                    obj.put(column_name, rs.getString(column_name));
                } else if (rsmd.getColumnType(i) == java.sql.Types.TINYINT) {
                    obj.put(column_name, rs.getInt(column_name));
                } else if (rsmd.getColumnType(i) == java.sql.Types.SMALLINT) {
                    obj.put(column_name, rs.getInt(column_name));
                } else if (rsmd.getColumnType(i) == java.sql.Types.DATE) {
                    obj.put(column_name, rs.getDate(column_name));
                } else if (rsmd.getColumnType(i) == java.sql.Types.TIMESTAMP) {
                    obj.put(column_name, rs.getTimestamp(column_name));
                } else {
                    obj.put(column_name, rs.getObject(column_name));
                }
            }

            json.put(obj);
        }

        return json;
    }

    private static Connection connection = null;

    /* Inicializa la conexión a la base de datos,
	 * pero no la devuelve como resultado
    */
    public void iniciarConexion(){
        //String url = "jdbc:postgresql://localhost:5432/AQUI EL NOMBRE DE LA BASE DE WILL";
        //String url = "jdbc:postgresql://localhost:5432/AQUI EL NOMBRE DE LA BASE DE VLADI";
        String url = "jdbc:postgresql://localhost:5432/WevMapping";
        String pass = "admin";
        //String pass = CONTRASEÑA DE VLADI;
        //String pass = CONTRASEÑA DE WILL;
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, "postgres", pass);
            System.out.println("Conexión exitosa");
            
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Problemas de conexión " + e);
            e.printStackTrace();
        }

    }

    public Connection getConnection() {
        iniciarConexion();
        return connection;
    }
    
    /* Realiza las acciones necesarias para finalizar la conexión a 
	 * la base de datos
    */
    public void finalizarConexion() {
		
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}	//se cierra la conexion 
	}

  
}

