/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wevmapping.entities;

/**
 *
 * @author Vladimir
 */
public class Person {
    
    int personID;
    String email;
    String name;
    String password;
    boolean isManager;

    public Person(int personID, String email, String name, String password, boolean isManager) {
        this.personID = personID;
        this.email = email;
        this.name = name;
        this.password = password;
        this.isManager = isManager;
    }
    
    public Person(){
        
    }

    public int getPersonID() {
        return personID;
    }

    public void setPersonID(int personID) {
        this.personID = personID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isIsManager() {
        return isManager;
    }

    public void setIsManager(boolean isManager) {
        this.isManager = isManager;
    }

    @Override
    public String toString() {
        return "Person{" + "personID=" + personID + ", email=" + email + ", name=" + name + ", password=" + password + ", isManager=" + isManager + '}';
    }
    
}
