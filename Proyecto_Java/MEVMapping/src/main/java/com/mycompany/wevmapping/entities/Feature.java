/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wevmapping.entities;

import java.sql.Timestamp;
import java.util.ArrayList;

/**
 *
 * @author Vladimir
 */
public class Feature {
    
    int featureID;
    int layerID;
    String layerName;
    String title;
    Timestamp timeStamp;
    ArrayList<Input> arregloInput;
    int userID;

    public Feature(int featureID, int layerID, String title, Timestamp timeStamp, ArrayList<Input> arregloInput, int userID) {
        this.featureID = featureID;
        this.layerID = layerID;
        this.title = title;
        this.timeStamp = timeStamp;
        this.arregloInput = arregloInput;
        this.userID = userID;
    }

    public Feature() {
    }

    public String getLayerName() {
        return layerName;
    }

    public void setLayerName(String layerName) {
        this.layerName = layerName;
    }

    
    
    public int getFeatureID() {
        return featureID;
    }

    public void setFeatureID(int featureID) {
        this.featureID = featureID;
    }
    

    public int getLayerID() {
        return layerID;
    }

    public void setLayerID(int layerID) {
        this.layerID = layerID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Timestamp getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Timestamp timeStamp) {
        this.timeStamp = timeStamp;
    }

    public ArrayList<Input> getArregloInput() {
        return arregloInput;
    }

    public void setArregloInput(ArrayList<Input> arregloInput) {
        this.arregloInput = arregloInput;
    }
    
    public String getInput(Input input) {
        return input.getName();
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
}
