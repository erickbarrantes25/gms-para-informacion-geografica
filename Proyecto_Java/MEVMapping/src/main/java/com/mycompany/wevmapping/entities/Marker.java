/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wevmapping.entities;

import java.util.ArrayList;

/**
 *
 * @author williamsalazaresquivel
 */
public class Marker {
    String title;
    ArrayList<Coordinate> coordinates;
    String type;

    public Marker(String title, ArrayList<Coordinate> coordinates, String type) {
        this.title = title;
        this.coordinates = coordinates;
        this.type = type;
    }

    public Marker() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<Coordinate> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(ArrayList<Coordinate> coordinates) {
        this.coordinates = coordinates;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    
    
    
}
