/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wevmapping.entities;

import java.util.ArrayList;


public class Layer {
    
    int layerid;
    String layername;
    ArrayList<Field> fieldArray;

    public Layer(int ID_Layer, String nombreLayer, ArrayList<Field> fieldArray) {
        this.layerid = ID_Layer;
        this.layername = nombreLayer;
        this.fieldArray = fieldArray;
    }
    
    public Layer() {
        
    }

    public ArrayList<Field> getFieldArray() {
        return fieldArray;
    }

    public void setFieldArray(ArrayList<Field> fieldArray) {
        this.fieldArray = fieldArray;
    }
    
    public int getlayerid() {
        return layerid;
    }

    public void setlayerid(int ID_Layer) {
        this.layerid = ID_Layer;
    }

    public String getlayername() {
        return layername;
    }

    public void setlayername(String nombreLayer) {
        this.layername = nombreLayer;
    }

    @Override
    public String toString() {
        return "Layer{" + "layerid=" + layerid + ", nombreLayer=" + layername + '}';
    }
}
