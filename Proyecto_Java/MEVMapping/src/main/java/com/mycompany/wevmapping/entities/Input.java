/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wevmapping.entities;

/**
 *
 * @author Vladimir
 */
public class Input {
    
    String name;
    String type;

    public Input(int inputID, String name, String type, int featureID) {
        
        this.name = name;
        this.type = type;
        
    }

    public Input() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    
}
