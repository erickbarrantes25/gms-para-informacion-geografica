package com.mycompany.wevmapping.gestores;


import com.mycompany.wevmapping.entities.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;
import java.sql.ResultSetMetaData;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.postgresql.Driver;

public class Consultas {
    
    /* Convierte el contenido de un ResultSet 
	 en un objeto de tipo JSONArray
    */
    public static JSONArray convert(ResultSet rs)
            throws SQLException, JSONException {
        JSONArray json = new JSONArray();
        ResultSetMetaData rsmd = rs.getMetaData();

        while (rs.next()) {
            int numColumns = rsmd.getColumnCount();
            JSONObject obj = new JSONObject();

            for (int i = 1; i < numColumns + 1; i++) {
                String column_name = rsmd.getColumnName(i);

                if (rsmd.getColumnType(i) == java.sql.Types.ARRAY) {
                    obj.put(column_name, rs.getArray(column_name));
                } else if (rsmd.getColumnType(i) == java.sql.Types.BIGINT) {
                    obj.put(column_name, rs.getInt(column_name));
                } else if (rsmd.getColumnType(i) == java.sql.Types.BOOLEAN) {
                    obj.put(column_name, rs.getBoolean(column_name));
                } else if (rsmd.getColumnType(i) == java.sql.Types.BLOB) {
                    obj.put(column_name, rs.getBlob(column_name));
                } else if (rsmd.getColumnType(i) == java.sql.Types.DOUBLE) {
                    obj.put(column_name, rs.getDouble(column_name));
                } else if (rsmd.getColumnType(i) == java.sql.Types.FLOAT) {
                    obj.put(column_name, rs.getFloat(column_name));
                } else if (rsmd.getColumnType(i) == java.sql.Types.INTEGER) {
                    obj.put(column_name, rs.getInt(column_name));
                } else if (rsmd.getColumnType(i) == java.sql.Types.NVARCHAR) {
                    obj.put(column_name, rs.getNString(column_name));
                } else if (rsmd.getColumnType(i) == java.sql.Types.VARCHAR) {
                    obj.put(column_name, rs.getString(column_name));
                } else if (rsmd.getColumnType(i) == java.sql.Types.TINYINT) {
                    obj.put(column_name, rs.getInt(column_name));
                } else if (rsmd.getColumnType(i) == java.sql.Types.SMALLINT) {
                    obj.put(column_name, rs.getInt(column_name));
                } else if (rsmd.getColumnType(i) == java.sql.Types.DATE) {
                    obj.put(column_name, rs.getDate(column_name));
                } else if (rsmd.getColumnType(i) == java.sql.Types.TIMESTAMP) {
                    obj.put(column_name, rs.getTimestamp(column_name));
                } else {
                    obj.put(column_name, rs.getObject(column_name));
                }
            }

            json.put(obj);
        }

        return json;
    }
    
    /* Realiza consulta a base de datos y retorna
	 todos los fields correspondientes a una
         capa en específic y ru respectiva informacion
    */
    public ArrayList<Layer> consultarCapas(ConexionPostgre cp) throws SQLException, JSONException {
        ArrayList<Layer> array = new ArrayList<>();
        Connection con = cp.getConnection();
        String query = "SELECT layername, layerid FROM layer;";
        
        try{
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            while (rs.next()) {
                Layer l = new Layer();
                l.setlayername(rs.getString("layername"));
                l.setlayerid(rs.getInt("layerid"));
                array.add(l);
            }
           
        }catch (SQLException e) {
            System.out.println(e);
        }
       return array;
    }
    
    public ArrayList<Field> consultarField(ConexionPostgre cp, int fieldId){
        Field f = new Field();
        ArrayList<Field> array = new ArrayList<>();
        Connection con = cp.getConnection();
        String query = "SELECT fieldid, instruction, fieldtype, required, multivaluated " 
                + "FROM field where fieldid = '" + fieldId + "';";
        
        try{
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            
            while(rs.next()){
                f.setFieldID(rs.getInt("fieldid"));
                f.setInstruction(rs.getString("instruction"));
                f.setMultivaluated(rs.getBoolean("multivaluated"));
                
                f.setType(rs.getString("fieldtype"));
                f.setRequired(rs.getBoolean("required"));
                array.add(f);
            }
            
        }catch (SQLException e) {
            System.out.println(e);
        }finally{
            cp.finalizarConexion();
        }
        return array;
    }
    
    public ArrayList<Field> listaFieldsLayer(ConexionPostgre cp, String nombre) throws SQLException, JSONException {
        JSONArray res = new JSONArray();
        ArrayList<Field> array = new ArrayList<>();
        Connection con = cp.getConnection();
        String query = "SELECT fieldid, instruction, fieldtype, required, multivaluated, "
        + "layername FROM layer inner join field ON field.LayerBelonging=layer.LayerID "
                + "where layername = '" + nombre + "';";
        
        try{
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            
            while (rs.next()) {
                Field l = new Field();
                
                l.setFieldID(rs.getInt("fieldid"));
                l.setInstruction(rs.getString("instruction"));
                l.setMultivaluated(rs.getBoolean("multivaluated"));
                
                l.setType(rs.getString("fieldtype"));
                l.setRequired(rs.getBoolean("required"));
                array.add(l);

            }
            
            res = convert(rs);
        } catch (SQLException e) {
            System.out.println(e);
        }
       return array;
    }
    
    /* Realiza inserción a base de datos de la
        información de un usuario nuevo
    */
    public boolean registrarUsuario(ConexionPostgre cp, String userName, String email, String pass) throws SQLException{
        Connection con = cp.getConnection();
        boolean bandera = false;
        String query = "INSERT INTO normaluser(email, username, pass)"+
        " VALUES('"+email+"','"+userName+"','"+pass+"');";
        try{
            Statement s = con.createStatement();
            int res = s.executeUpdate(query);
            if(res == 1){
                System.out.println("Insertado correctamente");
                bandera = true;
                return bandera;
            }
            else{
                System.out.println("Error al insertar");
            }
        }catch(SQLException e){
            System.out.println(e);
        }
        return bandera;
    }
    
    /* Realiza inserción a base de datos de la
        información de ua capa nueva
    */
    public boolean insertarCapa(ConexionPostgre cp, String layerName) throws SQLException{
        boolean bandera = false;
        Connection con = cp.getConnection();
        String query = "INSERT INTO layer(LayerName)"+
        " VALUES('"+layerName+"');";
        try{
            Statement s = con.createStatement();
            int res = s.executeUpdate(query);
            if(res == 1){
                bandera = true;
                System.out.println("Insertado correctamente");
                return bandera;
            }
            else{
                System.out.println("Error al insertar");
            }
        }catch(SQLException e){
            System.out.println(e);
        }
        return bandera;
    }
    
    /* Realiza inserción a base de datos de la
        información de un nuevo campo
    */
    public boolean insertarField(ConexionPostgre cp, String layer, ArrayList<Field> array) throws SQLException{
        boolean b = false;
        Connection con = cp.getConnection();
        
        String id = null;
        String query2 = "SELECT layerid FROM layer WHERE"+" layername= '"+layer+"';";
        Statement c = con.createStatement();
        ResultSet rs = c.executeQuery(query2);
        while (rs.next()) {
            id = rs.getString("layerid");
        }
        int num_layer = Integer.parseInt(id);
        
        for(int x = 0; x < array.size(); x++){
            String query = "INSERT INTO field(layerbelonging, initialName, instruction, fieldtype, required, multivaluated)"+
            " VALUES ("+num_layer+", '"+array.get(x).getInitialName()+"', '"+array.get(x).getInstruction()+"', '"+array.get(x).getType()+"', "+array.get(x).getRequired()+", "+array.get(x).getMultivaluated()+");";
            try{
                Statement s = con.createStatement();
                int res = s.executeUpdate(query);
                System.out.println("Insertado correctamente");
                b = true;
                createFieldTables(cp, num_layer, array);
            }catch(SQLException e){
                System.out.println(e);
                b = false;
            }
        }
        
        return b;
    }
    
    public void createFieldTables(ConexionPostgre cp, int layer, ArrayList<Field> array){
        Connection con = cp.getConnection();
        boolean bandera = false;
        for(int x = 0; x < array.size(); x++){
            if(!array.get(x).getInitialName().equals("Location")){
                String query = "drop table if exists "+array.get(x).getInitialName()+"_"+layer+";\n"
                    + "drop sequence if exists "+array.get(x).getInitialName()+"_"+layer+"_seq;\n"
                    + "create sequence "+array.get(x).getInitialName()+"_"+layer+"_seq;\n"
                    + "create table "+array.get(x).getInitialName()+"_"+layer+"(\n"
                    + "\tfeatureID int unique,\n"
                    + "\tlayerID int,\n"
                    + "\tid int default nextval('"+array.get(x).getInitialName()+"_"+layer+"_seq'),\n"
                    + "\t"+array.get(x).getInitialName()+" varchar(500),\n"  //Cuando es la locación se elimina esta columna
                    + "\tprimary key (featureID, id),\n"
                    + "\tforeign key (featureID, layerID) references feature(InsertedFeatureID, Layer) on delete cascade\n"
                    + ");";
                System.out.println(query);
                try{
                    Statement s = con.createStatement();
                    int res = s.executeUpdate(query);
                    bandera = true;
                    System.out.println("Tabla creada correctamente");
                }catch(SQLException e){
                    System.out.println(e);
                    bandera= false;
                }
            }else {
                String query = "drop table if exists "+array.get(x).getInitialName()+"_"+layer+";\n"
                    + "drop sequence if exists "+array.get(x).getInitialName()+"_"+layer+"_seq;\n"
                    + "create sequence "+array.get(x).getInitialName()+"_"+layer+"_seq;\n"
                    + "create table "+array.get(x).getInitialName()+"_"+layer+"(\n"
                    + "\tfeatureID int unique,\n"
                    + "\tlayerID int,\n"
                    + "\tid int default nextval('"+array.get(x).getInitialName()+"_"+layer+"_seq'),\n"
                    + "\t"+array.get(x).getInitialName()+" geography("+ array.get(x).getType()+"),\n"    
                    + "\tprimary key (featureID, id),\n"
                    + "\tforeign key (featureID, layerID) references feature(InsertedFeatureID, Layer) on delete cascade\n"
                    + ");";
                System.out.println(query);
                try{
                    Statement s = con.createStatement();
                    int res = s.executeUpdate(query);
                    bandera = true;
                    
                    System.out.println("Tabla Location creada correctamente");
                }catch(SQLException e){
                    System.out.println(e);
                    bandera= false;
                }
                
                
            }
            
        }
       // return bandera;
    }
    
 
    
    /* Realiza actualización a base de datos de la
        información de un campo
    */
    public boolean actualizarField(ConexionPostgre cp, int fieldID, Field update) throws SQLException{
        System.out.println(fieldID);
        boolean b = false;
        Connection con = cp.getConnection();
        String query = "UPDATE public.field SET instruction='"+update.getInstruction()+"', fieldtype='"+update.getType()+"', required="+update.getRequired()+", multivaluated="+update.getMultivaluated()
                + " WHERE fieldid="+fieldID+";";
        System.out.println(query);
        try{
            Statement s = con.createStatement();
            int res = s.executeUpdate(query);
            if(res == 1){
                b = true;
                System.out.println("Actualizado correctamente");

            }
            else{
                System.out.println("Error al Actualizar");
            }
        }catch(SQLException e){
            System.out.println(e);
        }finally{
            return b;
        }
        
    }
    
    /* Elimina de la base de datos la
        información de un campo
    */
    public boolean eliminarField(ConexionPostgre cp, int fieldID) throws SQLException{
        boolean b= false;
        Connection con = cp.getConnection();
        String query = "DELETE FROM field WHERE fieldid ="+fieldID+";";
        try{
            Statement s = con.createStatement();
            int res = s.executeUpdate(query);
            if(res == 1){
                b = true;
                System.out.println("Eliminado correctamente");
            }
            else{
                System.out.println("Error al Eliminar");
            }
        }catch(SQLException e){
            System.out.println(e);
        }
        finally{
            return b;
        }
    }
   
    /* Realiza actualización a base de datos de la
        información de una capa
    */
    public boolean actualizarCapa(ConexionPostgre cp, int layerid, String layerNameNew) throws SQLException{
        boolean b = false;
        Connection con = cp.getConnection();
        String query = "UPDATE layer SET layername='"+layerNameNew+"' "
                + "WHERE layerid= "+layerid+";";
        try{
            Statement s = con.createStatement();
            int res = s.executeUpdate(query);
            if(res == 1){
                b = true;
                System.out.println("Actualizado correctamente");
            }
            else{
                System.out.println("Error al Actualizar");
            }
        }catch(SQLException e){
            System.out.println(e);
        }
        return b;    
    }
    
    /* Elimina de la base de datos la
        información de una capa
    */
    public boolean eliminarCapa(ConexionPostgre cp, int layerid) throws SQLException{
        boolean b = false;
        Connection con = cp.getConnection();
        String query = "DELETE FROM layer WHERE layerid = "+layerid+";";
        try{
            Statement s = con.createStatement();
            int res = s.executeUpdate(query);
            if(res == 1){
                b = true;
                System.out.println("Eliminado correctamente");
            }
            else{
                System.out.println("Error al Eliminar");
            }
        }catch(SQLException e){
            System.out.println(e);
        }
        return b;
    }
    
    /* Verifica que la información insertada 
    en un formulario de inicio de sesion sea 
    la correcta segun los registros de la base 
    de Datos
    */
    public boolean[] confirmarUsuario(ConexionPostgre cp, String userName, String pass){
        boolean response[] = new boolean[2];
        boolean res = false;
        boolean manager= false;
        Connection con = cp.getConnection();
        String query = "SELECT username, pass, manager FROM normaluser where"
                + " username= '"+userName+"';";
        try{
            String password = null, user;
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            while (rs.next()) {
                user = rs.getString("UserName");
                password = rs.getString("Pass");
                manager = rs.getBoolean("manager");
            }
            if(pass.equals(password)){
                res = true;
                response[0]=res;
                response[1]=manager;
                
            }
            
        }catch(SQLException e){
            System.out.println(e);
        }
        return response;
    }
    
    public int getUsuario(ConexionPostgre cp, String userName, String pass){
        int userid = 0;
        Connection con = cp.getConnection();
        String query = "SELECT userid FROM normaluser where"
                + " username= '"+userName+"';";
        try{
            String password = null, user;
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            while (rs.next()) {
                userid = rs.getInt("userid");

            }

            
        }catch(SQLException e){
            System.out.println(e);
        }
        return userid;
    }
    
    public Person consultarUserInfo(ConexionPostgre cp, int userid){
        
        Connection con = cp.getConnection();
        Person p = new Person();
        String query = "SELECT username, pass, email FROM normaluser where"
                + " userid= '"+userid+"';";
        try{
            
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            while (rs.next()) {
                p.setName(rs.getString("username"));
                p.setEmail(rs.getString("email"));
                p.setPassword(rs.getString("pass"));

            }

            
        }catch(SQLException e){
            System.out.println(e);
        }
        return p;
    }
    
    
    public boolean actualizarUsuario(ConexionPostgre cp, String userName, String email, String password, int id) throws SQLException{
        boolean b = false;
        Connection con = cp.getConnection();
        String query = "UPDATE normaluser SET email = '"+email+"', pass='"+password+"', username= '"+userName+"' "
                + "WHERE userid= '"+id+"'"+";";
        try{
            Statement s = con.createStatement();
            int res = s.executeUpdate(query);
            if(res == 1){
                b = true;
                System.out.println("Actualizado correctamente");
            }
            else{
                System.out.println("Error al Actualizar");
            }
        }catch(SQLException e){
            System.out.println(e);
        }
        return b;    
    }
    
    public boolean insertarFeature(ConexionPostgre cp, int layer, ArrayList<Input> array, String title, int user) throws SQLException{
        boolean b = false;
        Connection con = cp.getConnection();
       
            String query = "INSERT INTO feature(layer, title, ts, userid)"+
            " VALUES ("+layer+", '"+title+"', current_timestamp, "+user+");";
            try{
                Statement s = con.createStatement();
                int res = s.executeUpdate(query);
                System.out.println("Insertado correctamente");
                b = true;
            }catch(SQLException e){
                System.out.println(e);
                b = false;
            }
            
            String id = null;
            String query2 = "SELECT insertedfeatureid FROM feature where"
                + " title= '"+title+"';";
            Statement c = con.createStatement();
            ResultSet rs = c.executeQuery(query2);
            while (rs.next()) {
                id = rs.getString("insertedfeatureid");
            }
            String type = null;
            String querytype = "SELECT fieldtype FROM field where layerbelonging = " + layer + "AND initialname = 'Location';";
            Statement c1 = con.createStatement();
            rs = c1.executeQuery(querytype);
            while (rs.next()) {
                type = rs.getString("fieldtype");
            }
            
            for(int x = 0; x < array.size(); x++){
                String query3;
               try{ 
                if((array.get(x).getType().equals("Location") || array.get(x).getType().equals("location")) && (type.equals("POINT") || type.equals("LINESTRING")) ){
                    query3 = "INSERT INTO "+array.get(x).getType()+"_"+layer+"(featureid, layerid, "+array.get(x).getType()+")"+
                " VALUES ("+id+","+layer+",ST_GeomFromText('"+type+"("+ array.get(x).getName()+")', 4326));";
                }else if(array.get(x).getType().equals("Location") || array.get(x).getType().equals("location")){
                    query3 = "INSERT INTO "+array.get(x).getType()+"_"+layer+"(featureid, layerid, "+array.get(x).getType()+")"+
                    " VALUES ("+id+","+layer+",ST_GeomFromText('"+type+"(("+ array.get(x).getName()+"))', 4326));";
                }else{
                    query3 = "INSERT INTO "+array.get(x).getType()+"_"+layer+"(featureid, layerid, "+array.get(x).getType()+")"+
                " VALUES ("+id+","+layer+", '"+array.get(x).getName()+"');";
                }
                System.out.println(query3);
                Statement a = con.createStatement();
                int res = a.executeUpdate(query3);
                System.out.println("Insertado correctamente");
                b = true;
                
                }catch(SQLException e){
                    System.out.println(e);
                    b = false;
                }
                
            }
        return b;
    }
    
    
 
    
    
    
     public ArrayList<Feature> consultarTodosFeature(ConexionPostgre cp){
         Connection con = cp.getConnection();
         ArrayList<String> layersName = new ArrayList<>();
         ArrayList<Integer> layers = new ArrayList<>();
         ArrayList<Feature> features = new ArrayList<>();
         ArrayList<Input> resultado = new ArrayList<>();
         ArrayList<String> fields = new ArrayList<>();
         ArrayList<String> title = new ArrayList<>();
         ArrayList<Integer> indices = new ArrayList<>();
         String query2 = "Select layerid, layername from layer";
        
        try{
            Statement s = con.createStatement();
            ResultSet res = s.executeQuery(query2);
            while (res.next()) {
                layers.add(res.getInt("layerid"));
                layersName.add(res.getString("layername"));
            }
        }catch(SQLException e){
            System.out.println(e);
        }
        
        
         
        for(int j = 0; j < layers.size(); j++){ 
            System.out.println("Inicia layer con "+j);
            fields.clear();
            title.clear();
            indices.clear();
            String query = "Select initialname from field where layerbelonging ="+layers.get(j)+";";
                try{
                    Statement s = con.createStatement();
                    ResultSet res = s.executeQuery(query);
                    while (res.next()) {
                        fields.add(res.getString("initialname"));
                    }
                }catch(SQLException e){
                    System.out.println(e);
                }
                
                
                
                String query4 = "Select title, insertedfeatureid from feature where layer ="+layers.get(j)+";";
        
                try{
                    Statement s = con.createStatement();
                    ResultSet res = s.executeQuery(query4);
                    while (res.next()) {
                        title.add(res.getString("Title"));
                        indices.add(res.getInt("insertedFeatureId"));  
                    }
                }catch(SQLException e){
                    System.out.println(e);
                }
                
                
                for(int c = 0; c < indices.size(); c++){
                   resultado.clear();
                   System.out.println("Inicia");
                    for(int i = 0; i < fields.size(); i++){
                        try{
                            Statement s = con.createStatement();
                            String query3;
                            if(fields.get(i).equals("Location") || fields.get(i).equals("location")){
                                query3 = "Select   ST_AsText(location) as Location from "+fields.get(i)+"_"+layers.get(j)+" where featureid="+indices.get(c)+";";
                                System.out.println(query3);
                            }else{
                                query3 = "Select "+fields.get(i)+" from "+fields.get(i)+"_"+layers.get(j)+" where featureid="+indices.get(c)+";";
                            }//System.out.println(query3);
                            ResultSet res = s.executeQuery(query3);
                            while (res.next()) {
                                Input entrada = new Input();
                                entrada.setName(res.getString(fields.get(i)));
                                entrada.setType(fields.get(i));
                                resultado.add(entrada);
                            }
                        }catch(SQLException e){
                            System.out.println(e);
                        }
                    }
                    ArrayList<Input> copia = new ArrayList<>();
                    for(int x=0;x<resultado.size();x++) {
                        copia.add(resultado.get(x));
                    }
                    Feature feat = new Feature();
                    feat.setLayerName(layersName.get(j));
                    feat.setTitle(title.get(c));
                    feat.setFeatureID(indices.get(c));
                    feat.setArregloInput(copia);
                    feat.setLayerID(layers.get(j));
                    features.add(feat);
                    
                }
        }
        /*
        System.out.println();
        for(int x=0;x<features.size();x++) {
            ArrayList<Input> list = new ArrayList();
            list = features.get(x).getArregloInput();
            System.out.println(features.get(x).getTitle());
            for(int q=0;q<list.size();q++){
                System.out.println(list.get(q).getName());
                System.out.println(list.get(q).getType());
                System.out.println();
            }  
        }*/
        return features;
    }
     
     
    public ArrayList<String> consultarFeature(ConexionPostgre cp) throws SQLException, JSONException {
        ArrayList<String> array = new ArrayList<>();
        Connection con = cp.getConnection();
        String query = "SELECT title FROM feature;";
        
        try{
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            while (rs.next()) {
                array.add(rs.getString("title"));
            }
           
        }catch (SQLException e) {
            System.out.println(e);
        }
       return array;
    }
    
    public ArrayList<Marker> consultarMarker(ConexionPostgre cp, int layerList[]) throws SQLException, JSONException{
        Connection con = cp.getConnection();
        ArrayList<Marker> arreglo = new ArrayList<>();
        for(int i=0; i<layerList.length;i++){
            String stringJson = null, name= null;
            String query = "SELECT ST_AsGeoJSON(location), title FROM location_"+layerList[i]+", feature " +
            "WHERE location_"+layerList[i]+".featureid = feature.insertedfeatureid AND feature.layer = "+layerList[i]+";";
            try{
                Statement s = con.createStatement();
                ResultSet rs = s.executeQuery(query);
                 while (rs.next()) {
                    stringJson = rs.getString("st_asgeojson");
                    name = rs.getString("title");
                    JSONObject jsonObj = new JSONObject(stringJson);
                    ArrayList<Coordinate> arregloCoor = new ArrayList<>();
                    arregloCoor = hacerCoordenadas(jsonObj.getString("coordinates"));
                    Marker marcador = new Marker();
                    marcador.setType(jsonObj.getString("type"));
                    marcador.setCoordinates(arregloCoor);
                    marcador.setTitle(name);
                    arreglo.add(marcador);
                }           
            }catch(SQLException e){
                System.out.println(e);
            }
        }
        return arreglo;
    }
    
    public ArrayList<Coordinate> hacerCoordenadas(String coor){
        int largo = coor.length();
      
        
        coor = coor.replace("[","");
        coor = coor.replace("]","");
  
        
        ArrayList<Coordinate> arreglo = new ArrayList<>();
        
        String[] numeros = coor.split(",");
        for(int i = 0; i<numeros.length; i += 2){
            Coordinate nuevo = new Coordinate();
            nuevo.setLat(Double.parseDouble(numeros[i]));
            nuevo.setLng(Double.parseDouble(numeros[i+1]));
            arreglo.add(nuevo);
        }
        
        return arreglo;
    }
    
    
    
    
    
    
    
    public boolean eliminarFeature(ConexionPostgre cp, int featureid) throws SQLException{
        boolean b= false;
        Connection con = cp.getConnection();
        
        String query = "DELETE FROM feature WHERE feature.insertedfeatureid ="+featureid+";";
        try{
            Statement s = con.createStatement();
            int res = s.executeUpdate(query);
            System.out.println(res);
            if(res == 1){
                b = true;
                System.out.println("Eliminado correctamente");
            }
            else{
                System.out.println("Error al Eliminar");
            }
        }catch(SQLException e){
            System.out.println(e);
        }
        finally{
            return b;
        }
    }
    
    public boolean actualizarFeature(ConexionPostgre cp, String feat, String feature) throws SQLException{
        boolean b= false;
        Connection con = cp.getConnection();
        String query = "UPDATE feature SET title='"+feature+"' "
                + "WHERE title= '"+feat+"'"+";";
        try{
            Statement s = con.createStatement();
            int res = s.executeUpdate(query);
            System.out.println(res);
            if(res == 1){
                b = true;
                System.out.println("Acttualizado correctamente");
            }
            else{
                System.out.println("Error al Actualizar");
            }
        }catch(SQLException e){
            System.out.println(e);
        }
        finally{
            return b;
        }
    }
    
    public boolean actualizarInput(ConexionPostgre cp, String feature, String name, String type) throws SQLException{
        boolean b= false;
        Connection con = cp.getConnection();
        String insert = null, layer = null ;
        String query = "Select insertedfeatureid, layer from feature WHERE title='"+feature+"';";
        try{
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            while (rs.next()) {
                insert = rs.getString("insertedfeatureid");
                layer = rs.getString("layer");
            }
        }catch(SQLException e){
            System.out.println(e);
        }
        
        System.out.println(insert);
        System.out.println(layer);
        String query2;
        
        if(type.equals("Location")){
            query2 = "UPDATE "+type+"_"+layer+" SET "+type+"= ST_GeomFromText('"+ name+ "',4326) WHERE featureid= '"+insert+"';";
        }else{
            query2 = "UPDATE "+type+"_"+layer+" SET "+type+"='"+name+"' "
            + "WHERE featureid= '"+insert+"';";
        
        }
        
        
        System.out.println(query2);
        
        try{
            Statement s = con.createStatement();
            int res = s.executeUpdate(query2);
            System.out.println(res);
            if(res == 1){
                b = true;
                System.out.println("Eliminado correctamente");
            }
            else{
                System.out.println("Error al Eliminar");
            }
        }catch(SQLException e){
            System.out.println(e);
        }
        finally{
            return b;
        }
    }
    
    
    public static void main(String[] args) throws SQLException, JSONException {
        ConexionPostgre a = new ConexionPostgre();
        JSONArray capas = new JSONArray();
        Consultas c = new Consultas();
        //JSONArray r = c.consultarCapas(a, "Volcanoes");
        c.actualizarInput(a, "Volcan Irazu", "Prueba", "nombre");
    }
    
}

