/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wevmapping.entities;

/**
 *
 * @author Vladimir
 */
public class Field {
    
    int fieldID;
    int layerBelonging;
    String initialName;
    String instruction;
    String type;
    boolean required;
    boolean multivaluated;
    

    public Field(int fieldID, int layerBelonging, String initialName, String instruction, String type, boolean required, boolean multivaluated) {
        this.fieldID = fieldID;
        this.layerBelonging = layerBelonging;
        this.initialName = initialName;
        this.instruction = instruction;
        this.type = type;
        this.required = required;
        this.multivaluated = multivaluated;
    }
    
    public Field(){
        
    }

    public String getInitialName() {
        return initialName;
    }

    public void setInitialName(String initialName) {
        this.initialName = initialName;
    }
    
    public int getFieldID() {
        return fieldID;
    }

    public void setFieldID(int fieldID) {
        this.fieldID = fieldID;
    }

    public int getLayerBelonging() {
        return layerBelonging;
    }

    public void setLayerBelonging(int layerBelonging) {
        this.layerBelonging = layerBelonging;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String intruction) {
        this.instruction = intruction;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean getRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean getMultivaluated() {
        return multivaluated;
    }

    public void setMultivaluated(boolean multivaluated) {
        this.multivaluated = multivaluated;
    }

    @Override
    public String toString() {
        return "Field{" + "fieldID=" + fieldID + ", layerBelonging=" + layerBelonging + ", intruction=" + instruction + ", type=" + type + ", requeried=" + required + ", multivaluated=" + multivaluated + '}';
    }
    
    
    
}
