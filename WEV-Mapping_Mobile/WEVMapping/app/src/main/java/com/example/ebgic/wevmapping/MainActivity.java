package com.example.ebgic.wevmapping;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpResponseException;
import org.ksoap2.transport.HttpTransportSE;
import org.ksoap2.transport.HttpsTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

public class MainActivity extends AppCompatActivity {

    private String userName;
    private int id;
    private EditText user, pass;
    private Button log, sign;
    private TextView resultadoConsulta;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        user = (EditText)findViewById(R.id.editTextUser);
        pass = (EditText)findViewById(R.id.editTextPass);
        log = (Button)findViewById(R.id.logIn);
        sign = (Button)findViewById(R.id.signUp);
        resultadoConsulta = (TextView)findViewById(R.id.result);
        resultadoConsulta.setText("");
        log.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new operacionSoap().execute(user.getText().toString().trim(), pass.getText().toString().trim());
            }
        });
        sign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Registro.class));

            }
        });
    }

    private class operacionSoap extends AsyncTask<String, Void, ArrayList<String>>{
        static final String NAMESPACE = "http://webService/";
        static final String METHODNAME = "confirmarUsuario";
        static final String URL = "http://192.168.1.143:8080/consultasService/consultas?WSDL";
        static final String SOAP_ACTION = NAMESPACE+METHODNAME;

        @Override
        protected ArrayList<String> doInBackground(String... strings) {
            String des = null;
            ArrayList<String> res = new ArrayList<>();
            SoapObject request = new SoapObject(NAMESPACE, METHODNAME);
            request.addProperty("user", strings[0]);
            request.addProperty("pass", strings[1]);
            SoapSerializationEnvelope envelope =  new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = false;
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte = new HttpTransportSE(URL);

            try{
                transporte.call(SOAP_ACTION, envelope);
                Vector<SoapPrimitive> result = (Vector<SoapPrimitive>) envelope.getResponse();
                SoapPrimitive so1 = result.get(0);
                SoapPrimitive so2 = result.get(1);
                SoapPrimitive so3 = result.get(2);
                userName = so1.toString();
                Log.d("usuario", userName);
                id = Integer.parseInt(so2.toString());
                Log.d("id", String.valueOf(id));
                des = so3.toString();

                res.add(userName);
                res.add(String.valueOf(id));
                res.add(des);

                }catch (Exception e){
                Log.d("eXXX", e.getMessage());
            }
            return res;
        }

        @Override
        protected void onPostExecute(ArrayList<String> array) {
            super.onPostExecute(array);
            String msj;
            if(array.get(2).equals("true")){
                msj = "¡WELCOME!";
            }
            else{
                msj = "INVALID USERNAME OR PASSWORD";
            }
            resultadoConsulta.setText(msj);
            resultadoConsulta.setVisibility(View.VISIBLE);
            if(msj.equals("¡WELCOME!")){
                Intent intent = new Intent(MainActivity.this, Layers.class);
                Bundle extras = new Bundle();
                extras.putString("userName", array.get(0));
                extras.putString("iden", array.get(1));
                intent.putExtras(extras);
                startActivity(intent);
                }
            else{
                user.setText("");
                pass.setText("");
            }
        }
    }

}


