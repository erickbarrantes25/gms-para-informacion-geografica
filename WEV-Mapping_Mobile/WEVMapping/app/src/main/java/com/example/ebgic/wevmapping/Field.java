package com.example.ebgic.wevmapping;

public class Field {

    int fieldID;
    int layerBelonging;
    String initialName;
    String intruction;
    String type;
    boolean requeried;
    int multivaluated;

    public Field(int fieldID, int layerBelonging, String initialName, String intruction, String type, boolean requeried, int multivaluated) {
        this.fieldID = fieldID;
        this.layerBelonging = layerBelonging;
        this.initialName = initialName;
        this.intruction = intruction;
        this.type = type;
        this.requeried = requeried;
        this.multivaluated = multivaluated;
    }

    public Field(){

    }

    public String getInitialName() {
        return initialName;
    }

    public void setInitialName(String initialName) {
        this.initialName = initialName;
    }

    public int getFieldID() {
        return fieldID;
    }

    public void setFieldID(int fieldID) {
        this.fieldID = fieldID;
    }

    public int getLayerBelonging() {
        return layerBelonging;
    }

    public void setLayerBelonging(int layerBelonging) {
        this.layerBelonging = layerBelonging;
    }

    public String getIntruction() {
        return intruction;
    }

    public void setIntruction(String intruction) {
        this.intruction = intruction;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean getRequeried() {
        return requeried;
    }

    public void setRequeried(boolean requeried) {
        this.requeried = requeried;
    }

    public int getMultivaluated() {
        return multivaluated;
    }

    public void setMultivaluated(int multivaluated) {
        this.multivaluated = multivaluated;
    }

    @Override
    public String toString() {
        return "Field{" + "fieldID=" + fieldID + ", layerBelonging=" + layerBelonging + ", intruction=" + intruction + ", type=" + type + ", requeried=" + requeried + ", multivaluated=" + multivaluated + '}';
    }



}

