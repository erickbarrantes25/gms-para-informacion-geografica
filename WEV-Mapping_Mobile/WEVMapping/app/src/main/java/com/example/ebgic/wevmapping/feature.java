package com.example.ebgic.wevmapping;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.Vector;

public class feature extends AppCompatActivity {
    private TextView ti;
    ArrayList<String> inst;
    private ListView list;
    ArrayAdapter<String> adaptador;
    private Button b;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_feature);
        b = (Button) findViewById(R.id.button2);
        list = (ListView) findViewById(R.id.list);
        inst = new ArrayList<>();
        inst.add("Fields");
        adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, inst);
        list.setAdapter(adaptador);
        list.setVisibility(View.VISIBLE);
        String nombre = getIntent().getStringExtra("EXTRA");
        String title = "Insert feature in " + nombre + " layer";
        ti = (TextView) findViewById(R.id.titulo);
        ti.setText(title);
        new operacionSoap3().execute(nombre.trim());
        b.setOnClickListener(new View.OnClickListener(){
            @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
            @Override
            public void onClick(View view){
                finish();
            }
        });


    }

    private class operacionSoap3 extends AsyncTask<String, Void, ArrayList<Field>> {
        static final String NAMESPACE = "http://webService/";
        static final String METHODNAME = "consultarFieldsLayer";
        static final String URL = "http://192.168.1.143:8080/consultasService/consultas?WSDL";
        static final String SOAP_ACTION = NAMESPACE + METHODNAME;

        @Override
        protected ArrayList<Field> doInBackground(String... strings) {
            inst.clear();
            ArrayList<Field> res = new ArrayList<>();
            SoapObject request = new SoapObject(NAMESPACE, METHODNAME);
            request.addProperty("layer", strings[0]);
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = false;
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte = new HttpTransportSE(URL);
            try {
                transporte.call(SOAP_ACTION, envelope);
                Vector<SoapObject> result = (Vector<SoapObject>) envelope.getResponse();
                int tamaño = result.size();
                Log.d("Fields", String.valueOf(tamaño));
                int i = 0;
                while(i < tamaño){
                    Field campo = new Field();
                    SoapObject so = (SoapObject) result.get(i);
                    campo.setIntruction(so.getProperty("intruction").toString());
                    campo.setType(so.getProperty("type").toString());
                    campo.setLayerBelonging(Integer.parseInt(so.getProperty("layerBelonging").toString()));
                    res.add(campo);
                    i++;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println(res.size());
            return res;
        }

        @Override
        protected void onPostExecute(ArrayList<Field> campos) {
        for(int i = 0; i < campos.size(); i++){
            inst.add(campos.get(i).getIntruction()+"  ------   "+campos.get(i).getType());
            }
            adaptador.notifyDataSetChanged();

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

}
