package com.example.ebgic.wevmapping;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpResponseException;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import static android.R.layout.simple_spinner_item;

public class Layers extends AppCompatActivity {
    private Button refresh,filter;
    private String usuario, id;
    private ListView list;
    ArrayList<String> capas;
    ArrayAdapter<String> adaptador;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layers);

        Bundle extras = getIntent().getExtras();
        if(extras!=null){
            usuario = extras.getString("userName");
            id = extras.getString("iden");
        }
        Log.d("user", usuario);
        Log.d("iden", id);
        list = (ListView) findViewById(R.id.lista);
        capas = new ArrayList<>();
        capas.add("List of Layers");
        adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, capas);
        list.setAdapter(adaptador);
        list.setVisibility(View.VISIBLE);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String seleccion = (String) parent.getAdapter().getItem(position);

                Intent intent = new Intent(getBaseContext(), feature.class);

                intent.putExtra("EXTRA", seleccion);

                startActivity(intent);
            }
        });
        refresh = (Button)findViewById(R.id.ref);
        refresh.setOnClickListener(new View.OnClickListener(){
            @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
            @Override
            public void onClick(View view){
                new operacionSoap2().execute();
                Log.d("entre", "click");
            }
        });

        filter = (Button)findViewById(R.id.fil);
        filter.setOnClickListener(new View.OnClickListener(){
            @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
            @Override
            public void onClick(View view){
                new operacionSoap3().execute(id.trim());
            }
        });
    }

    private  class operacionSoap3 extends AsyncTask<String, Void, ArrayList<String>>{
        static final String NAMESPACE = "http://webService/";
        static final String METHODNAME = "consultarCapasFiltradas";
        static final String URL = "http://192.168.1.143:8080/consultasService/consultas?WSDL";
        static final String SOAP_ACTION = NAMESPACE+METHODNAME;

        @Override
        protected ArrayList<String> doInBackground(String... strings){
            capas.clear();
            ArrayList<String> res = new ArrayList<>();
            SoapObject request = new SoapObject(NAMESPACE, METHODNAME);
            request.addProperty("identificador", strings[0]);
            SoapSerializationEnvelope envelope =  new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = false;
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte = new HttpTransportSE(URL);
            try {
                transporte.call(SOAP_ACTION, envelope);
                Vector<SoapPrimitive> result = (Vector<SoapPrimitive>) envelope.getResponse();
                int tamaño = result.size();
                Log.d("Hay", String.valueOf(tamaño));
                int i = 0;
                while(i < tamaño){

                    SoapPrimitive so = (SoapPrimitive) result.get(i);
                    Log.d("Hay", so.toString() );
                    capas.add(so.toString());
                    System.out.println(capas.get(i));
                    i++;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return capas;
        }
        @Override
        protected void onPostExecute(ArrayList<String> capas) {
            if(capas.size()==0){
                capas.add("You haven't worked on any layer");
                adaptador.notifyDataSetChanged();
            }
            else{
                adaptador.notifyDataSetChanged();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    private class operacionSoap2 extends AsyncTask<Void, Void, ArrayList<String>>{
        static final String NAMESPACE = "http://webService/";
        static final String METHODNAME = "consultarCapas";
        static final String URL = "http://192.168.1.143:8080/consultasService/consultas?WSDL";
        static final String SOAP_ACTION = NAMESPACE+METHODNAME;

        @Override
        protected ArrayList<String> doInBackground(Void...  voids) {
            capas.clear();
            SoapObject request = new SoapObject(NAMESPACE, METHODNAME);
            SoapSerializationEnvelope envelope =  new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = false;
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte = new HttpTransportSE(URL);
            try {
                transporte.call(SOAP_ACTION, envelope);
                Vector<SoapPrimitive> result = (Vector<SoapPrimitive>) envelope.getResponse();
                int tamaño = result.size();
                Log.d("Hay", String.valueOf(tamaño));
                int i = 0;
                while(i < tamaño){

                    SoapPrimitive so = (SoapPrimitive) result.get(i);
                    Log.d("Hay", so.toString() );
                    capas.add(so.toString());
                    System.out.println(capas.get(i));
                    i++;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return capas;
        }
        @Override
        protected void onPostExecute(ArrayList<String> capas) {

            adaptador.notifyDataSetChanged();
        }
    }
}
