package webService;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Vladimir
 */
public class Layer {
    
    int layerid;
    String layername;

    public Layer(int ID_Layer, String nombreLayer) {
        this.layerid = ID_Layer;
        this.layername = nombreLayer;
    }
    
    public Layer() {
        
    }
    
    public int getlayerid() {
        return layerid;
    }

    public void setlayerid(int ID_Layer) {
        this.layerid = ID_Layer;
    }

    public String getlayername() {
        return layername;
    }

    public void setlayername(String nombreLayer) {
        this.layername = nombreLayer;
    }

    @Override
    public String toString() {
        return "Layer{" + "layerid=" + layerid + ", nombreLayer=" + layername + '}';
    }
}
