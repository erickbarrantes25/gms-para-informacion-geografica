package webService;

import java.sql.ResultSetMetaData;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.postgresql.Driver;

public class Consultas_1{
    
    /* Convierte el contenido de un ResultSet 
	 en un objeto de tipo JSONArray
    */
  
    
    /* Realiza consulta a base de datos y retorna
	 todos los fields correspondientes a una
         capa en específic y ru respectiva informacion
    */
    public ArrayList<String> consultarCapas(ConexionPostgre cp) throws SQLException {
        ArrayList<String> array = new ArrayList<>();
        Connection con = cp.getConnection();
        String query = "SELECT layername FROM layer;";
        
        try{
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            while (rs.next()) {
                String l = rs.getString("layername");
                array.add(l);
            }
           
        }catch (SQLException e) {
            System.out.println(e);
        }
       return array;
    }
    
    public ArrayList<String> consultarCapasFiltradas(ConexionPostgre cp, String id) throws SQLException {
        ArrayList<String> array = new ArrayList<>();
        Connection con = cp.getConnection();
        String query = "SELECT layer FROM feature where iduser = "+id+" group by layer;";
        ArrayList<String> capas = new ArrayList<>();
        
        try{
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            while (rs.next()) {
                String idCapa = rs.getString("layer");
                capas.add(idCapa);
            }
           
        }catch (SQLException e) {
            System.out.println(e);
        }
        for(int i = 0; i < capas.size(); i++){
            String query2 = "SELECT layername FROM layer where layerid = "+capas.get(i)+";";
            try{
                Statement s = con.createStatement();
                ResultSet rs = s.executeQuery(query2);
                while (rs.next()) {
                    String l = rs.getString("layername");
                    array.add(l);
                }
           }catch (SQLException e) {
            System.out.println(e);
            }
        }
       return array;
    }
    public ArrayList<Field> listaFieldsLayer(ConexionPostgre cp, String nombre) throws SQLException{
        ArrayList<Field> array = new ArrayList<>();
        Connection con = cp.getConnection();
        String query = "SELECT fieldid, instruction, fieldtype, required, multivaluated, "
        + "layername FROM layer inner join field ON field.LayerBelonging=layer.LayerID "
                + "where layername = '" + nombre + "';";
        try{
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            
            while (rs.next()) {
                Field l = new Field();
                l.setFieldID(rs.getInt("fieldid"));
                l.setIntruction(rs.getString("instruction"));
                l.setMultivaluated(rs.getInt("multivaluated"));
                l.setType(rs.getString("fieldtype"));
                l.setRequeried(rs.getBoolean("required"));
                array.add(l);
            }   
        } catch (SQLException e) {
            System.out.println(e);
        }
       return array;
    }
    
    /* Realiza inserción a base de datos de la
        información de un usuario nuevo
    */
    public boolean registrarUsuario(ConexionPostgre cp, String userName, String email, String pass) throws SQLException{
        Connection con = cp.getConnection();
        boolean bandera = false;
        String query = "INSERT INTO normaluser(email, username, pass)"+
        " VALUES('"+email+"','"+userName+"','"+pass+"');";
        try{
            Statement s = con.createStatement();
            int res = s.executeUpdate(query);
            if(res == 1){
                System.out.println("Insertado correctamente");
                bandera = true;
                return bandera;
            }
            else{
                System.out.println("Error al insertar");
            }
        }catch(SQLException e){
            System.out.println(e);
        }
        return bandera;
    }
    
    /* Realiza inserción a base de datos de la
        información de ua capa nueva
    */
    public boolean insertarCapa(ConexionPostgre cp, String layerName) throws SQLException{
        boolean bandera = false;
        Connection con = cp.getConnection();
        String query = "INSERT INTO layer(LayerName)"+
        " VALUES('"+layerName+"');";
        try{
            Statement s = con.createStatement();
            int res = s.executeUpdate(query);
            if(res == 1){
                bandera = true;
                System.out.println("Insertado correctamente");
                return bandera;
            }
            else{
                System.out.println("Error al insertar");
            }
        }catch(SQLException e){
            System.out.println(e);
        }
        return bandera;
    }
    
    /* Realiza inserción a base de datos de la
        información de un nuevo campo
    */
    
    public boolean insertarField(ConexionPostgre cp, String layer, ArrayList<Field> array) throws SQLException{
        boolean b = false;
        Connection con = cp.getConnection();
        
        String id = null;
        String query2 = "SELECT layerid FROM layer WHERE"+" layername= '"+layer+"';";
        Statement c = con.createStatement();
        ResultSet rs = c.executeQuery(query2);
        while (rs.next()) {
            id = rs.getString("layerid");
        }
        int num_layer = Integer.parseInt(id);
        
        for(int x = 0; x < array.size(); x++){
            String query = "INSERT INTO field(layerbelonging, initialName, instruction, fieldtype, required, multivaluated)"+
            " VALUES ("+num_layer+", '"+array.get(x).getInitialName()+"', '"+array.get(x).getIntruction()+"', '"+array.get(x).getType()+"', "+array.get(x).getRequeried()+", "+array.get(x).getMultivaluated()+");";
            try{
                Statement s = con.createStatement();
                int res = s.executeUpdate(query);
                System.out.println("Insertado correctamente");
                b = true;
                createFieldTables(cp, num_layer, array);
            }catch(SQLException e){
                System.out.println(e);
                b = false;
            }
        }
        
        return b;
    }
    
    public void createFieldTables(ConexionPostgre cp, int layer, ArrayList<Field> array){
        Connection con = cp.getConnection();
        boolean bandera = false;
        for(int x = 0; x < array.size(); x++){
            String query = "drop table if exists "+array.get(x).getInitialName()+"_"+layer+";\n"
                + "drop sequence if exists "+array.get(x).getInitialName()+"_"+layer+"_seq;\n"
                + "create sequence "+array.get(x).getInitialName()+"_"+layer+"_seq;\n"
                + "create table "+array.get(x).getInitialName()+"_"+layer+"(\n"
                + "\tfeatureID int,\n"
                + "layerID int,\n"
                + "\tid int default nextval('"+array.get(x).getInitialName()+"_"+layer+"_seq'),\n"
                + "\t"+array.get(x).getInitialName()+" varchar(500),\n"
                + "\tprimary key (featureID, id),\n"
                + "\tforeign key (featureID, layerID) references feature(InsertedFeatureID, Layer) on delete cascade\n"
                + ");";
            System.out.println(query);
            try{
                Statement s = con.createStatement();
                int res = s.executeUpdate(query);
                bandera = true;
                System.out.println("Tabla creada correctamente");
            }catch(SQLException e){
                System.out.println(e);
                bandera= false;
            }
        }
       // return bandera;
    }
    
 
    
    /* Realiza actualización a base de datos de la
        información de un campo
    */
    public boolean actualizarField(ConexionPostgre cp, int fieldID, Field update) throws SQLException{
        boolean b = false;
        Connection con = cp.getConnection();
        String query = "UPDATE field SET instruction='"+update.getIntruction()+"', fieldtype='"+update.getType()+"', required="+update.getRequeried()+", multivaluated="+update.getMultivaluated()
                + "WHERE fieldid="+fieldID+";";
        try{
            Statement s = con.createStatement();
            int res = s.executeUpdate(query);
            if(res == 1){
                b = true;
                System.out.println("Actualizado correctamente");

            }
            else{
                System.out.println("Error al Actualizar");
            }
        }catch(SQLException e){
            System.out.println(e);
        }finally{
            return b;
        }
        
    }
    
    /* Elimina de la base de datos la
        información de un campo
    */
    public boolean eliminarField(ConexionPostgre cp, int fieldID) throws SQLException{
        boolean b= false;
        Connection con = cp.getConnection();
        String query = "DELETE FROM field WHERE fieldid ="+fieldID+";";
        try{
            Statement s = con.createStatement();
            int res = s.executeUpdate(query);
            if(res == 1){
                b = true;
                System.out.println("Eliminado correctamente");
            }
            else{
                System.out.println("Error al Eliminar");
            }
        }catch(SQLException e){
            System.out.println(e);
        }
        finally{
            return b;
        }
    }
   
    /* Realiza actualización a base de datos de la
        información de una capa
    */
    public boolean actualizarCapa(ConexionPostgre cp, int layerid, String layerNameNew) throws SQLException{
        boolean b = false;
        Connection con = cp.getConnection();
        String query = "UPDATE layer SET layername='"+layerNameNew+"' "
                + "WHERE layerid= "+layerid+";";
        try{
            Statement s = con.createStatement();
            int res = s.executeUpdate(query);
            if(res == 1){
                b = true;
                System.out.println("Actualizado correctamente");
            }
            else{
                System.out.println("Error al Actualizar");
            }
        }catch(SQLException e){
            System.out.println(e);
        }
        return b;    
    }
    
    /* Elimina de la base de datos la
        información de una capa
    */
    public boolean eliminarCapa(ConexionPostgre cp, int layerid) throws SQLException{
        boolean b = false;
        Connection con = cp.getConnection();
        String query = "DELETE FROM layer WHERE layerid = "+layerid+";";
        try{
            Statement s = con.createStatement();
            int res = s.executeUpdate(query);
            if(res == 1){
                b = true;
                System.out.println("Eliminado correctamente");
            }
            else{
                System.out.println("Error al Eliminar");
            }
        }catch(SQLException e){
            System.out.println(e);
        }
        return b;
    }
    
    /* Verifica que la información insertada 
    en un formulario de inicio de sesion sea 
    la correcta segun los registros de la base 
    de Datos
    */
    public ArrayList<String> confirmarUsuario(ConexionPostgre cp, String userName, String pass){
        ArrayList<String> res = new ArrayList<>();
        Connection con = cp.getConnection();
        String query = "SELECT username, pass, userid FROM normaluser where"
                + " username= '"+userName+"';";
        try{
            String password = null, user, id;
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            while (rs.next()) {
                user = rs.getString("UserName");
                res.add(user);
                password = rs.getString("Pass");
                id = String.valueOf(rs.getInt("userid"));
                res.add(id);
            }
            if(pass.equals(password)){
                res.add("true");
                return res;
            }
            else{
                res.add("false");
            }
        }catch(SQLException e){
            System.out.println(e);
        }
        return res;
    }
    
    
    public boolean actualizarUsuario(ConexionPostgre cp, String userName, String email, String password) throws SQLException{
        boolean b = false;
        Connection con = cp.getConnection();
        String query = "UPDATE normaluser SET email = '"+email+"', pass='"+password+"' "
                + "WHERE username= '"+userName+"'"+";";
        try{
            Statement s = con.createStatement();
            int res = s.executeUpdate(query);
            if(res == 1){
                b = true;
                System.out.println("Actualizado correctamente");
            }
            else{
                System.out.println("Error al Actualizar");
            }
        }catch(SQLException e){
            System.out.println(e);
        }
        return b;    
    }
    
    public boolean insertarFeature(ConexionPostgre cp, int layer, ArrayList<Input> array, String title, int user) throws SQLException{
        boolean b = false;
        Connection con = cp.getConnection();
       
            String query = "INSERT INTO feature(layer, title, ts, userid)"+
            " VALUES ("+layer+", '"+title+"', current_timestamp, "+user+");";
            try{
                Statement s = con.createStatement();
                int res = s.executeUpdate(query);
                System.out.println("Insertado correctamente");
                b = true;
            }catch(SQLException e){
                System.out.println(e);
                b = false;
            }
            
            String id = null;
            String query2 = "SELECT insertedfeatureid FROM feature where"
                + " title= '"+title+"';";
            Statement c = con.createStatement();
            ResultSet rs = c.executeQuery(query2);
            while (rs.next()) {
                id = rs.getString("insertedfeatureid");
            }
            
            
            for(int x = 0; x < array.size(); x++){
               try{ 
                String query3 = "INSERT INTO "+array.get(x).getType()+"_"+layer+"(featureid, layerid, "+array.get(x).getType()+")"+
                " VALUES ("+id+","+layer+", '"+array.get(x).getName()+"');";
                System.out.println(query3);
                Statement a = con.createStatement();
                int res = a.executeUpdate(query3);
                System.out.println("Insertado correctamente");
                b = true;
                
                }catch(SQLException e){
                    System.out.println(e);
                    b = false;
                }
                
            }
        return b;
    }
    
    
    public ArrayList<Input> consultarFeature(ConexionPostgre cp, int idFeature, int layerID){
        ArrayList<Input> resultado = new ArrayList<>();
        Connection con = cp.getConnection();
        ArrayList<String> fields = new ArrayList<>();
        String query = "Select shortInst from Field where layerbelonging ="+layerID+");";
            try{
                Statement s = con.createStatement();
                ResultSet res = s.executeQuery(query);
                while (res.next()) {
                    fields.add(res.getString("shortinstr"));
                }
                System.out.println("Insertado correctamente");
            }catch(SQLException e){
                System.out.println(e);
            }
            for(int i = 0; i < fields.size(); i++){
                try{
                Statement s = con.createStatement();
                String query2 = "Select "+fields.get(i)+" from "+fields.get(i)+"_"+layerID+" where featureid="+idFeature+";";
                ResultSet res = s.executeQuery(query2);
                while (res.next()) {
                    Input entrada = new Input();
                    entrada.setName(res.getString(fields.get(i)));
                    entrada.setType(fields.get(i));
                    resultado.add(entrada);
                    }
                }catch(SQLException e){
                    System.out.println(e);
                }
            }
        return resultado;
    }

    
    
    public static void main(String[] args) throws SQLException{
        ConexionPostgre a = new ConexionPostgre();
        Consultas_1 c = new Consultas_1();
        //JSONArray r = c.consultarCapas(a, "Volcanoes");
        //c.insertarCapa(a, "Perreras");
        ArrayList<String> arr = c.consultarCapasFiltradas(a, "1");
        for(int i = 0; i < arr.size(); i++){
            System.out.println(arr.get(i));
        }
   
    }
    
}

