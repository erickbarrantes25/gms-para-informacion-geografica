package webService;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Vladimir
 */
public class Input {
    
    int inputID;
    String name;
    String type;
    int featureID;

    public Input(int inputID, String name, String type, int featureID) {
        this.inputID = inputID;
        this.name = name;
        this.type = type;
        this.featureID = featureID;
    }

    public Input() {
    }

    public int getInputID() {
        return inputID;
    }

    public void setInputID(int inputID) {
        this.inputID = inputID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getFeatureID() {
        return featureID;
    }

    public void setFeatureID(int featureID) {
        this.featureID = featureID;
    }

    @Override
    public String toString() {
        return "Input{" + "inputID=" + inputID + ", name=" + name + ", type=" + type + ", featureID=" + featureID + '}';
    }
    
    
}
