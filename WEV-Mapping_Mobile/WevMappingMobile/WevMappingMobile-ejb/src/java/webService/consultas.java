/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author ebgic
 */

@WebService
@Stateless
@LocalBean
public class consultas {

    /**
     * Web service operation
     */
    @WebMethod(operationName = "confirmarUsuario")
    public ArrayList<String> confirmarUsuario(@WebParam(name = "user") String user, @WebParam(name = "pass") String pass) {
        //TODO write your implementation code here:
        ConexionPostgre a = new ConexionPostgre();
        Consultas_1 b = new Consultas_1();
        ArrayList<String> res = b.confirmarUsuario(a, user, pass);
        return res;
    }
    
    @WebMethod(operationName = "consultarCapas")
    public ArrayList<String> consultarCapas(){
    ConexionPostgre a = new ConexionPostgre();
    Consultas_1 b = new Consultas_1();
    ArrayList<String> array = null;
        try {
            array = b.consultarCapas(a);
        } catch (SQLException ex) {
            Logger.getLogger(consultas.class.getName()).log(Level.SEVERE, null, ex);
        }
    return array;
    }
    
    @WebMethod(operationName = "consultarCapasFiltradas")
    public ArrayList<String> consultarCapasFiltradas(@WebParam(name = "identificador")String iden){
    ConexionPostgre a = new ConexionPostgre();
    Consultas_1 b = new Consultas_1();
    ArrayList<String> array = null;
        try {
            array = b.consultarCapasFiltradas(a, iden);
        } catch (SQLException ex) {
            Logger.getLogger(consultas.class.getName()).log(Level.SEVERE, null, ex);
        }
    return array;
    }
    
    @WebMethod(operationName = "consultarFieldsLayer")
    public ArrayList<Field> listaFieldsLayer(@WebParam(name = "layer")String layer){
    ConexionPostgre a = new ConexionPostgre();
    Consultas_1 b = new Consultas_1();
    ArrayList<Field> array = null;
        try {
            array = b.listaFieldsLayer(a, layer);
        } catch (SQLException ex) {
            Logger.getLogger(consultas.class.getName()).log(Level.SEVERE, null, ex);
        }
    return array;
    }
    
    
    
    
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}

