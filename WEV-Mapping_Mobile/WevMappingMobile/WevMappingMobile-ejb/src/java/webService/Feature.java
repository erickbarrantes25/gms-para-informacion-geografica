package webService;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.Timestamp;

/**
 *
 * @author Vladimir
 */
public class Feature {
    
    int insertedFeatureID;
    int layerID;
    String title;
    Timestamp timeStamp;

    public Feature(int insertedFeatureID, int layerID, String title, Timestamp timeStamp) {
        this.insertedFeatureID = insertedFeatureID;
        this.layerID = layerID;
        this.title = title;
        this.timeStamp = timeStamp;
    }

    public int getInsertedFeatureID() {
        return insertedFeatureID;
    }

    public void setInsertedFeatureID(int insertedFeatureID) {
        this.insertedFeatureID = insertedFeatureID;
    }

    public int getLayerID() {
        return layerID;
    }

    public void setLayerID(int layerID) {
        this.layerID = layerID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Timestamp getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Timestamp timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public String toString() {
        return "Feature{" + "insertedFeatureID=" + insertedFeatureID + ", layerID=" + layerID + ", title=" + title + ", timeStamp=" + timeStamp + '}';
    }
}
