package webService;



import org.postgresql.Driver;

 
import java.sql.ResultSetMetaData;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
;

public class ConexionPostgre {

    private static Connection connection = null;

    /* Inicializa la conexión a la base de datos,
	 * pero no la devuelve como resultado
    */
    public void iniciarConexion(){
        //String url = "jdbc:postgresql://localhost:5432/AQUI EL NOMBRE DE LA BASE DE WILL";
        //String url = "jdbc:postgresql://localhost:5432/AQUI EL NOMBRE DE LA BASE DE VLADI";
        String url = "jdbc:postgresql://localhost:5432/WevMapping";
        String pass = "ebgica";
        //String pass = CONTRASEÑA DE VLADI;
        //String pass = CONTRASEÑA DE WILL;
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, "postgres", pass);
            System.out.println("Conexión exitosa");
            
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Problemas de conexión " + e);
            e.printStackTrace();
        }

    }

    public Connection getConnection() {
        iniciarConexion();
        return connection;
    }
    
  
    /* Realiza las acciones necesarias para finalizar la conexión a 
	 * la base de datos
    */
    public void finalizarConexion() {
		
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}	//se cierra la conexion 
	}

  
}

