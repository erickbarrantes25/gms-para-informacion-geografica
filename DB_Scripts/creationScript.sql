  
drop table if exists normalUser;
drop sequence if exists user_sequence;
create sequence user_sequence;
create table normalUser(
	UserID int default nextval('user_sequence'),
    Email varchar(100) primary key,
    UserName varchar(100),
    Pass varchar(100)
);

drop table if exists manager;
drop sequence if exists manager_sequence;
create sequence manager_sequence;
create table manager(
	ManagerID int default nextval('manager_sequence'),
    Email varchar(100) primary key,
    ManagerName varchar(100),
    Pass varchar(100)
);

drop table if exists layer cascade;
drop sequence if exists layer_sequence;
create sequence layer_sequence;
create table layer(
	LayerID int default nextval('layer_sequence') primary key,
    LayerName varchar(100)
);

drop table if exists field;
drop sequence if exists field_sequence;
create sequence field_sequence;
drop type if exists tipo;
create type tipo as enum('int', 'float', 'bool', 'varchar()', 'date', 'multiple', 'punto', 'linea', 'poligono');
create table field(
	FieldID int default nextval('field_sequence'),
    LayerBelonging int not null,
    Instruction varchar(500),
    FieldType tipo,
    Required bool,
    Multivaluated int,
    primary key(FieldID, LayerBelonging),
    foreign key(LayerBelonging) references layer(LayerID)
);

drop table if exists feature;
drop sequence if exists feature_sequence;
create sequence feature_sequence;
create table feature(
	InsertedFeatureID int default nextval('feature_sequence'),
    Layer int not null,
    Title varchar(150),
    TS timestamp,
    primary key(InsertedFeatureID, Layer),
    foreign key(Layer) references layer(LayerID)
);









