import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataServiceService } from '../data-service.service';
import { HttpService } from '../http.service';
import { Feature } from '../new-feature/new-feature.component';

@Component({
  selector: 'app-feature-edit',
  templateUrl: './feature-edit.component.html',
  styleUrls: ['./feature-edit.component.css']
})
export class FeatureEditComponent implements OnInit {
  featureName: string;

  constructor(private router:Router, private data: DataServiceService,  public service:HttpService) { }

  ngOnInit() {
    this.data.currentMessage.subscribe(message => this.featureName = message);
  }

  save(e){
    if(window.confirm('¿Overwrite feature name?')){
      let newName = e.target.elements[0].value;
      let feature = new Feature(newName,0,null);
      this.service.updateFeatureName(this.featureName, feature);
      alert('Feature Name Updated');
      this.router.navigate(['dashboard/feature-list']);
    }
  }

  cancel(){
    this.router.navigate(['dashboard/feature-list']);
  }






}
