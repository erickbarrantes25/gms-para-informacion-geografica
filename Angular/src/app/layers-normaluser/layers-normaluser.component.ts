import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { HttpService } from '../http.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-layers-normaluser',
  templateUrl: './layers-normaluser.component.html',
  styleUrls: ['./layers-normaluser.component.css']
})
export class LayersNormaluserComponent implements OnInit {
  public layers$: Observable<any>;

  constructor(private router:Router,private user:UserService, public service:HttpService) 
  { }

  ngOnInit() {
    this.layers$ = this.service.getLayerList();
  }

}
