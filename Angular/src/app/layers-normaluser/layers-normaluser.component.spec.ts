import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayersNormaluserComponent } from './layers-normaluser.component';

describe('LayersNormaluserComponent', () => {
  let component: LayersNormaluserComponent;
  let fixture: ComponentFixture<LayersNormaluserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayersNormaluserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayersNormaluserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
