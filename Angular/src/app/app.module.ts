import { InputEditComponent } from './input-edit/input-edit.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { FooterComponent } from './footer/footer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RouterModule, Routes } from '@angular/router';
import { UserService } from './user.service';
import { AuthguardGuard } from './authguard.guard';
import { AdminpageComponent } from './adminpage/adminpage.component';
import { RegisterComponent } from './register/register.component';
import { MatExpansionModule, MatOptionModule, MatSelectModule, MatIconModule, MatDatepickerModule, MatNativeDateModule, MatCardModule, MatListModule, MatSelectionList } from '@angular/material';
import { MatFormFieldModule } from '@angular/material';
import { MatToolbarModule } from '@angular/material';
import { MatInputModule , MatButtonModule, MatButtonToggleModule} from '@angular/material';
import { AgmCoreModule } from '@agm/core';
import { NgModel } from '@angular/forms';



import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MapaComponent } from './mapa/mapa.component';
import { LayersComponent } from './layers/layers.component';
import { NewLayerComponent } from './new-layer/new-layer.component';
import { FieldComponent } from './field/field.component';
import { NewFeatureComponent } from './new-feature/new-feature.component';
import { SelectModule } from 'ng2-select';
import { FeatureComponent } from './feature/feature.component';
import { FeatureListComponent } from './feature-list/feature-list.component';
import { HttpClientModule } from '@angular/common/http';
import { HttpService } from './http.service';
import 'hammerjs';
import { EditLayerComponent } from './edit-layer/edit-layer.component';
import { DataServiceService } from './data-service.service';
import { FieldEditComponent } from './field-edit/field-edit.component';
import { FeatureEditComponent } from './feature-edit/feature-edit.component';
import { LayersNormaluserComponent } from './layers-normaluser/layers-normaluser.component';
import { UserEditComponent } from './user-edit/user-edit.component';

const appRoutes:Routes = [
  {
    path: '',
    component: LoginFormComponent
  },
  {
    path: 'dashboard',
    canActivate: [AuthguardGuard],    
    component: DashboardComponent,
    children: [
      { path: '', redirectTo: 'layers', pathMatch: 'full' },
      { 
        path: 'layers', 
        component: LayersNormaluserComponent },
      { path: 'new-feature', component: NewFeatureComponent },
      { path: 'feature-list', component: FeatureListComponent },
      { path: 'map', component: MapaComponent},
      { path: 'feature-edit', component: FeatureEditComponent},
      { path: 'input-edit', component: InputEditComponent},
      { path: 'user-edit',component: UserEditComponent}

    ]
  },
  {
    path: 'adminpage',
    canActivate: [AuthguardGuard],    
    component: AdminpageComponent,
    children: [
      { path: '', redirectTo: 'layers', pathMatch: 'full' },
      { path: 'layers', 
        component: LayersComponent,
        children: [
          { path: 'edit-layer', component: EditLayerComponent }
        ]
      },
      { path: 'new-layer', component: NewLayerComponent },
      { path: 'map', component: MapaComponent },
      { path: 'edit-layer', component: EditLayerComponent },
      { path: 'field-edit', component: FieldEditComponent}
      
    ]

    
  },
  {
    path: 'register',
    canActivate: [AuthguardGuard],    
    component: RegisterComponent

  }
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginFormComponent,
    FooterComponent,
    DashboardComponent,
    AdminpageComponent,
    RegisterComponent,
    LayersComponent,
    NewLayerComponent,
    FieldComponent,
    MapaComponent,
    NewFeatureComponent,
    FeatureComponent,
    FeatureListComponent,
    EditLayerComponent,
    FieldEditComponent,
    FeatureEditComponent,
    InputEditComponent,
    LayersNormaluserComponent,
    UserEditComponent
    
  ],
  imports: [
    BrowserModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    SelectModule,
    MatButtonToggleModule,
    MatToolbarModule,
    MatListModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAtGsdiRbyrHRLYJgoc5IH8JPzMzYf61no'
    }),
    RouterModule.forRoot(appRoutes)
  ],
  entryComponents: [FieldComponent,NewLayerComponent,FeatureComponent],
  providers: [UserService,AuthguardGuard, HttpService, DataServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
