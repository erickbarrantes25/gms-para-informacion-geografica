import { element } from 'protractor';
import { Component, 
  OnInit,
  ViewChild, 
  ComponentFactoryResolver,
  ViewContainerRef  } from '@angular/core';

import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { FieldComponent } from '../field/field.component';
import { FeatureComponent } from '../feature/feature.component';
import { Observable } from 'rxjs/Observable';
import { HttpService } from '../http.service';



@Component({
  selector: 'app-new-feature',
  templateUrl: './new-feature.component.html',
  styleUrls: ['./new-feature.component.css']
})
export class NewFeatureComponent implements OnInit {
  @ViewChild('parent', { read: ViewContainerRef }) container: ViewContainerRef;
  load(n){
    var comp = this._cfr.resolveComponentFactory(FeatureComponent);
    var featureComponent = this.container.createComponent(comp);
    featureComponent.instance._ref = featureComponent;

  }
  public layers$: Observable<any>;
  public fields$: Observable<any>;
  public inputArray: Array<Input>;
  public title: string = '';
  public layerID: number = 0;

  save(e){
    if(window.confirm('¿Save?')){
      this.inputArray = [];
      e.preventDefault();


      this.title = e.target.elements[0].value;

      console.log(e);
      let i : Input;
      
      for (var _i = 1; _i < e.target.elements.length-1; _i++){
        var name = e.target.elements[_i].value;
        var type = e.target.elements[_i].id; 
        i = new Input(name,type);
        this.inputArray.push(i);
        
      }
      let f: Feature;
      f = new Feature(this.title,1,this.inputArray);
      console.log(f);
      
      this.service.addFeature(this.layerID,f).subscribe((val)=>{
        alert(val);
        if(val=='Feature registrado correctamente'){
          this.router.navigate(['dashboard/feature-list'])
        }

      });
    }
    

    return false;
  }

  loadFields(layerName:string,layerID: number){
    this.fields$ = this.service.getFieldsList(layerName);
    this.layerID = layerID;
  }


  constructor(private _cfr: ComponentFactoryResolver,private router:Router,private user:UserService, public service:HttpService) { }

  ngOnInit() {
    this.layers$ = this.service.getLayerList();
  
    console.log(this.layers$);
  }


}


export class Feature{
  title: string = '';
  userID: number = 0;
  arregloInput: Array<Input>;


  constructor(title: string, userID: number ,arregloInput: Array<Input>) {

      this.title = title;
      this.userID = userID;
      this.arregloInput = arregloInput;


}
}
export class Input{
  name: string = '';
  type: string = '';


  constructor(name,type) {

      this.name = name;
      this.type = type;

}
}