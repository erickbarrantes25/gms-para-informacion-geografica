import { Injectable } from '@angular/core';

@Injectable()
export class UserService {
  private isUserLoggedIn;
  private username;
  private userid;

  constructor() {
    this.isUserLoggedIn = false;
   }
  
  setUserLoggedIn(){
    this.isUserLoggedIn = true;
  }
  getUserLoggedIn(){
    return this.isUserLoggedIn;
  }
  setUserID(id){
    this.userid = id;
  }
  getUserID(){
    return this.userid;
  }
}
