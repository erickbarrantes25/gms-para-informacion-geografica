import { Component, OnInit,   ViewChild, 
  ComponentFactoryResolver,
  ViewContainerRef   } from '@angular/core';
import { Router } from '@angular/router';
import { DataServiceService } from '../data-service.service';
import { Observable } from 'rxjs/Observable';
import { HttpService } from '../http.service';
import { FieldComponent } from '../field/field.component';
import { Field } from '../new-layer/new-layer.component';

@Component({
  selector: 'app-edit-layer',
  templateUrl: './edit-layer.component.html',
  styleUrls: ['./edit-layer.component.css']
})
export class EditLayerComponent implements OnInit {
  layername:string;
  layernameNew:string;
  layerid: string;
  location: string = '';
  fields:Array<Field>;
  public layer$: Observable<any>;

  @ViewChild('parent', { read: ViewContainerRef }) container: ViewContainerRef;

  constructor(private _cfr: ComponentFactoryResolver, private router:Router, private data: DataServiceService,  public service:HttpService) { }

  ngOnInit() {
    this.data.currentMessage.subscribe(message => this.layerid = message);
    this.data.currentMessageAux.subscribe(message => this.layername = message);
    console.log(this.layerid);



  }
  addComponent(){    
    var comp = this._cfr.resolveComponentFactory(FieldComponent);
    var fieldComponent = this.container.createComponent(comp);
    fieldComponent.instance._ref = fieldComponent;
  }

  save(e){
    if(window.confirm('Are you sure you want to overwrite Layer?')){
      this.fields = [];
      e.preventDefault();
      console.log(e.target.elements.length);

      this.layernameNew = e.target.elements[0].value;
      var locationType;
      let f;


      for (var _i = 1; _i < e.target.elements.length-2; _i = _i + 5) {
        var instruction = e.target.elements[_i].value;
        var type = e.target.elements[_i+1].value; 
        var required = e.target.elements[_i+2].checked;
        var multivaluated = e.target.elements[_i+3].checked;
        f = new Field(instruction,instruction,type,required,multivaluated);
        this.fields.push(f);
      }
      console.log(this.fields);
      let l = new Layer(this.layernameNew,this.layerid);
      this.service.updateLayerName(l).subscribe((val)=> {
        if(val=='Layer actualizado'){
          this.service.addFields(this.layernameNew,this.fields).subscribe((res)=> {
            
            if(res=='Layer actualizado'){
              alert('Layer updated')
              this.router.navigate(['adminpage/layers']);
            }else{
              alert('An error has ocurred, please check layer info')
            }

      
          });

        }
      });
    
    }
    
    

  }

}
export class Layer{
  layername: string = '';
  layerid: string;


  constructor(layername,layerid) {
      this.layername = layername;
      this.layerid = layerid;

}
}

