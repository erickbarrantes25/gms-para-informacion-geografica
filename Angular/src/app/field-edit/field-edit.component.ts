import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router } from '@angular/router';
import { DataServiceService } from '../data-service.service';
import { Observable } from 'rxjs/Observable';
import { Field } from '../new-layer/new-layer.component';

@Component({
  selector: 'app-field-edit',
  templateUrl: './field-edit.component.html',
  styleUrls: ['./field-edit.component.css']
})
export class FieldEditComponent implements OnInit {

  constructor(private router:Router, private data: DataServiceService,  public service:HttpService) { }
  fieldid:string;
  public field$: Observable<any>;

  
  ngOnInit() {
    this.data.currentMessage.subscribe(message => this.fieldid = message);
    this.field$ = this.service.getField(this.fieldid);
    console.log(this.field$);
  }

  save(e){
    if(window.confirm('¿Overwrite Field?')){
    console.log(e);
    let instruction = e.target.elements[0].value;
    let type = e.target.elements[1].value;
    let required = e.target.elements[2].checked;
    let multivaluated = e.target.elements[3].checked;
    let f = new Field(instruction,instruction,type,required,multivaluated);
   

    this.service.updateField(this.fieldid,f).subscribe((val)=> {
      
      if(val=='Field actualizado'){
        alert('Field updated');
        this.router.navigate(['adminpage/layers']);
      }else{
        alert('An error has ocurred, please check field info');
      }

    });
    }
  }
  cancel(){
    this.router.navigate(['layers']);
  }



}

