import { HttpService } from './../http.service';

import { Component, 
  OnInit,
  ViewChild, 
  ComponentFactoryResolver,
  ViewContainerRef  } from '@angular/core';

import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { FieldComponent } from '../field/field.component';


@Component({
  selector: 'app-new-layer',
  templateUrl: './new-layer.component.html',
  styleUrls: ['./new-layer.component.css']
})
export class NewLayerComponent implements OnInit {

  layername: string = '';
  location: string = '';
  fields:Array<Field>;




  @ViewChild('parent', { read: ViewContainerRef }) container: ViewContainerRef;
  
  constructor(private _cfr: ComponentFactoryResolver,private router:Router,private user:UserService,
  public service: HttpService) { }
  ngOnInit(){ }
  
  addComponent(){    
    var comp = this._cfr.resolveComponentFactory(FieldComponent);
    var fieldComponent = this.container.createComponent(comp);
    fieldComponent.instance._ref = fieldComponent;
  }



  save(e){
    if(window.confirm('¿Save?')){
    this.fields = [];
    e.preventDefault();
    console.log(e.target.elements[4]);
    console.log(e.target.elements.length);

    this.layername = e.target.elements[0].value;
    var locationType = e.target.elements[1].value;

    var f : Field;
    f = new Field('Location','Location',locationType,true,false);
    console.log(this.layername);
    console.log(f);
    this.fields.push(f);

    for (var _i = 2; _i < e.target.elements.length-2; _i = _i + 5) {
      var instruction = e.target.elements[_i].value;
      var type = e.target.elements[_i+1].value; 
      var required = e.target.elements[_i+2].checked;
      var multivaluated = e.target.elements[_i+3].checked;
      f = new Field(instruction,instruction,type,required,multivaluated);
      this.fields.push(f);
    }
    console.log(this.fields);
    this.service.addLayer(this.layername,this.fields).subscribe((val)=> {
      if(val=="Layer agregado"){
        alert('Layer Added');
        this.router.navigate(['adminpage/layers']);
      }else{
        alert('Can not add layer, check layer information');
      }
    });
   }
    
    

    return false;
  }





}

export class Field{
  initialName: string = '';
  instruction: string = '';
  type: string = '';
  required: boolean = false;
  multivaluated: boolean = false;

  constructor(initialName, instruction,type,required,multivaluated) {
      this.initialName = initialName;
      this.instruction = instruction;
      this.type = type;
      this.required = required;
      this.multivaluated = multivaluated;
}
}
