import { Observable } from 'rxjs/Observable';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';


// client
import { HttpService } from "../http.service";
import { DataServiceService } from '../data-service.service';

@Component({
  selector: 'app-layers',
  templateUrl: './layers.component.html',
  styleUrls: ['./layers.component.css']
})
export class LayersComponent implements OnInit {
  public layers$: Observable<any>;
  panelOpenState:  boolean = false;
  constructor(private router:Router,private user:UserService, public service:HttpService,
    private http: HttpClient, private data:DataServiceService) { }

  ngOnInit() {
    this.layers$ = this.service.getLayerList();
    console.log(this.layers$);
    
  }

  public deleteLayer(e){
    if(window.confirm('¿Are you sure you want to delete this Layer?')){
      this.service.deleteLayerById(e);
      alert('Layer Deleted');
    }  

  }

  deleteField(e){
    if(window.confirm('¿Are you sure you want to delete this Field?')){
      this.service.deleteFieldById(e);
      alert('Field Deleted');
    }  

  }
  public editLayer(e,name){
    this.data.changeMessage(e);
    this.data.changeMessageAux(name);
    console.log(this.data.currentMessage);
    this.router.navigate(['adminpage/edit-layer']);

  }
  addLayer(){
    this.router.navigate(['adminpage/new-layer']);
  }

  editField(e){
    this.data.changeMessage(e);
    this.router.navigate(['adminpage/field-edit']);
  }

}
