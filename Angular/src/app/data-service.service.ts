import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class DataServiceService {
  private messageSource = new BehaviorSubject<string>("default message");
  private messageArray = new BehaviorSubject<Array<string>>(['default message']);
  private messageAux = new BehaviorSubject<string>("default message");

  
  currentMessage = this.messageSource.asObservable();
  currentMessageArray = this.messageArray.asObservable();
  currentMessageAux = this.messageAux.asObservable();

  
  constructor() { }
  
  changeMessage(message: string) {
    this.messageSource.next(message)
  }

  changeMessageArray(message: Array<string>) {
    this.messageArray.next(message)
  }
  changeMessageAux(message:string) {
    this.messageAux.next(message)
  }


}
