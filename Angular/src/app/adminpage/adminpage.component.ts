import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import {MatExpansionModule} from '@angular/material/';

@Component({
  selector: 'app-adminpage',
  templateUrl: './adminpage.component.html',
  styleUrls: ['./adminpage.component.css'],
})
export class AdminpageComponent implements OnInit {
  
  constructor(private router:Router,private user:UserService) { 

  }

  ngOnInit() {
  }

}
