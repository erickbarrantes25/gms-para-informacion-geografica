import { Component, OnInit } from '@angular/core';
import { DataServiceService } from '../data-service.service';
import { Router } from '@angular/router';
import { HttpService } from '../http.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {
  username = 'will';
  email = 'will';
  pass= 'will';
  userid;
  info$;
  constructor(private router:Router,
    public service:HttpService, 
    private data: DataServiceService,
    private user: UserService) { }

  ngOnInit() {
    this.userid = this.user.getUserID();
    this.service.getUserInfo(this.userid).forEach((val)=>{
      this.info$ = val;
      this.username = this.info$.name;
      this.email = this.info$.email;
      this.pass = this.info$.password;
    })

  }

  save(e){
    this.username = e.target.elements[0].value;
    this.email = e.target.elements[1].value;
    this.pass = e.target.elements[2].value;
    if(window.confirm('¿Are you sure you want to edit profile info?')){
      this.service.editUser(this.username, this.email, this.pass,this.userid).subscribe((val)=>{
        if(val=="Usuario actualizado"){
          alert("Profile updated");
        }else{
          alert("An error has ocurred, check profile info");
        }
      });
    }
  }

}
