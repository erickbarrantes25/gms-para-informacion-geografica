import { Observable } from 'rxjs/Observable';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})
export class MapaComponent implements OnInit {
  //PosicionInicial
  public layers$: Observable<any>;
  markers$: Observable<any>;
  lat: number = 9.931653;

  lng: number = -84.096909;
  zoom: number = 10;

  
  constructor(private router:Router,private user:UserService,public service:HttpService) { }

  ngOnInit() {
    this.layers$ = this.service.getLayerList();

  }


  updateList(e){
    let layerList: number[]=[-1];
    for (var _i = 0; _i < e.selectedOptions.selected.length; _i++) {
      layerList[_i]=e.selectedOptions.selected[_i].value;
  }
    this.markers$ = this.service.getMarkersList(layerList);
  }


}
