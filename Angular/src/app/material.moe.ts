import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatExpansionModule, 
    MatOptionModule, 
    MatSelectModule, 
    MatIconModule, 
    MatDatepickerModule, 
    MatNativeDateModule,
    MatCardModule } from '@angular/material';
import { MatFormFieldModule, MatToolbarModule } from '@angular/material';
import { MatInputModule , 
    MatButtonModule} from '@angular/material';

@NgModule({
    imports: [MatButtonModule,
        MatExpansionModule,
        MatOptionModule, 
        MatSelectModule, 
        MatIconModule, 
        MatDatepickerModule, 
        MatNativeDateModule,
        MatInputModule,
        MatFormFieldModule,
        MatToolbarModule,
        MatCardModule],
    
    exports: [MatButtonModule,
        MatExpansionModule,
        MatOptionModule, 
        MatSelectModule, 
        MatIconModule, 
        MatDatepickerModule, 
        MatNativeDateModule,
        MatInputModule,
        MatFormFieldModule,
        MatToolbarModule,
        MatCardModule],
})

export class MaterialModule { }