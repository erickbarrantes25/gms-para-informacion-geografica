import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpService } from '../http.service';
import { Router } from '@angular/router';
import { DataServiceService } from '../data-service.service';

@Component({
  selector: 'app-feature-list',
  templateUrl: './feature-list.component.html',
  styleUrls: ['./feature-list.component.css']
})
export class FeatureListComponent implements OnInit {
  public features$: Observable<any>;
  array = new Array<string>();
  

  constructor(private router:Router,public service:HttpService, private data: DataServiceService) { }

  

  ngOnInit() {
    this.features$ = this.service.getFeatureList();
  }

  step = 0;
  
    setStep(index: number) {
      this.step = index;
    }
  
    nextStep() {
      this.step++;
    }
  
    prevStep() {
      this.step--;
    }

    public deleteFeature(e){
      if(window.confirm('¿Are you sure you want to deleted this Feature?')){
        this.service.deleteFeatureById(e);
        alert('Feature Deleted');
      }
  
    }

    editFeature(e){
      this.data.changeMessage(e);
      this.router.navigate(['dashboard/feature-edit']);
    }

    addFeature(){
      this.router.navigate(['dashboard/new-feature']);
    }

    editInput(title:string,type:string,name:string){
      this.array.push(title);
      this.array.push(type);
      this.array.push(name);
      this.data.changeMessageArray(this.array);
      this.router.navigate(['dashboard/input-edit']);
    }
}
