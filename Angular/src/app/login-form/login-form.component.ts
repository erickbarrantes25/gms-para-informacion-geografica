import { element } from 'protractor';
import { UserService } from './../user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router/';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  constructor(private router:Router,private user:UserService,public service:HttpService) { }

  ngOnInit() {
  }
  
  register(){
    this.router.navigate(['register']);
    
  }
  loginUser(e){
    
    console.log(e);
    e.preventDefault();
    console.log(e);
    var username = e.target.elements[0].value;
    var password = e.target.elements[1].value;
   
    let a= this.service.login(username,password);
    
    a.subscribe((val:string)=> {
      if(val == "Manager"){
        let id = this.service.getUserID(username).subscribe((val)=>{
          this.user.setUserID(val);
        });
        this.user.setUserLoggedIn();
        this.router.navigate(['adminpage']);
      }else if(val == "Normal"){
        let id = this.service.getUserID(username).subscribe((val)=>{
          this.user.setUserID(val); 
        });
        this.user.setUserLoggedIn();
        this.router.navigate(['dashboard']); 
      }else{
        alert('User Name or Password Incorrect');
      }


    });

    
    //alert(response);
   
    /*
    if(username =='admin' && password == 'admin'){
      this.user.setUserLoggedIn();
      this.router.navigate(['adminpage']);
    }else if(username =='usuariocomun' && password == '1234'){
      this.user.setUserLoggedIn();
      this.router.navigate(['dashboard']);
    } */
    
  }
}


