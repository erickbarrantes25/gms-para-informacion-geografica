import { element } from 'protractor';
import { Component, OnInit } from '@angular/core';
import { DataServiceService } from '../data-service.service';
import { HttpService } from '../http.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-input-edit',
  templateUrl: './input-edit.component.html',
  styleUrls: ['./input-edit.component.css']
})
export class InputEditComponent implements OnInit {
  featureName: string;
  type: string;
  value: string;
  newValue: string;

  constructor(private router:Router,public service:HttpService, private data: DataServiceService) { }
  array: Array<string>;
  ngOnInit() {
    this.data.currentMessageArray.subscribe(message => this.array = message);

    this.value = this.array.pop();
    this.type = this.array.pop();
    this.featureName = this.array.pop();

  }

  save(e){
    if(window.confirm('¿Overwrite input?')){
    this.newValue = e.target.elements[0].value;
    let input = new Input(this.type, this.newValue);
    this.service.updateInput(input, this.featureName).subscribe((val)=> {
      
      if(val=='Input actualizado'){
        alert('Input updated');
        this.router.navigate(['dashboard/feature-list']);

      }else {
        alert('Can not update, check input info');
      }

    });
    }
  

    

  }


}

export class Input{
  type: string;
  name: string;

  constructor(type,name){
    this.type = type;
    this.name = name;
  }



}
