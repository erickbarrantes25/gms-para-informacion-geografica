import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Http, Response,  Headers, URLSearchParams, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
import { Field } from './new-layer/new-layer.component';
import { Feature } from './new-feature/new-feature.component';
import { Layer } from './edit-layer/edit-layer.component';
import { Input } from './input-edit/input-edit.component';


@Injectable()
export class HttpService {
  constructor(public http: HttpClient) { }
  
  
  public getLayerList() {
    return this.http.get("http://localhost:8080/WEVMapping/rest/listLayer");
  };

  public getLayer(name:string) {
    return this.http.get("http://localhost:8080/WEVMapping/rest/listLayerFields/"+name);
  };

  public getFeatureList() {
    return this.http.get("http://localhost:8080/WEVMapping/rest/listFeatureInput");
  };

  public getFieldsList(layerName: string){
    return this.http.get("http://localhost:8080/WEVMapping/rest/listLayerFields/"+layerName);
  }

  public getField(fieldId:string) {
    return this.http.get("http://localhost:8080/WEVMapping/rest/field/"+fieldId);
  };
  public getUserInfo(userid) {
    return this.http.get("http://localhost:8080/WEVMapping/rest/login/userinfo/"+userid);
  };



  public getMarkersList( layerList:number[]){
    let json = JSON.stringify(layerList);
    let url = 'http://localhost:8080/WEVMapping/rest/map/markers/';
    let headers = new HttpHeaders().set('Content-Type','application/json');
    
    return this.http.post(url, json, {headers: headers});
    

  }

  public updateLayerName( l:Layer){
    let json = JSON.stringify(l);
    let url = 'http://localhost:8080/WEVMapping/rest/update/layer/';
    let headers = new HttpHeaders().set('Content-Type','application/json');
    
    return this.http.post(url, json, {headers: headers,responseType: 'text'});
    

  }

  public updateField( fieldid:string,f:Field){
    let json = JSON.stringify(f);
    let url = 'http://localhost:8080/WEVMapping/rest/update/field/'+fieldid;
    let headers = new HttpHeaders().set('Content-Type','application/json');
    
    return this.http.post(url, json, {headers: headers, responseType: 'text'});
    

  }

  public editUser(username,email,pass,userid){
    let p = new Person(email,pass,username);
    p.personID = userid
    let json = JSON.stringify(p);
    let url = 'http://localhost:8080/WEVMapping/rest/update/user/';
    let headers = new HttpHeaders().set('Content-Type','application/json');
    
    return this.http.post(url, json, {headers: headers, responseType: 'text'});
    

  }
  public updateFeatureName( name: string, feature: Feature){
    let json = JSON.stringify(feature);
    let url = 'http://localhost:8080/WEVMapping/rest/update/feature/'+name;
    let headers = new HttpHeaders().set('Content-Type','application/json');
    
    return this.http.post(url, json, {headers: headers}).subscribe((val)=> {
      console.log(val);

    });
    

  }
  public updateInput( input:Input, featureName:string){
    let json = JSON.stringify(input);
    let url = 'http://localhost:8080/WEVMapping/rest/update/input/'+featureName;
    let headers = new HttpHeaders().set('Content-Type','application/json');
    
    return this.http.post(url, json, {headers: headers, responseType: 'text'});
    

  }


  public addFields(layername:string, fields: Array<Field>){
    let json = JSON.stringify(fields);
    let url = 'http://localhost:8080/WEVMapping/rest/update/addNewFields/'+layername;
    let headers = new HttpHeaders().set('Content-Type','application/json');
    
    return this.http.post(url, json, {headers: headers,responseType: 'text'});
    

  }

  deleteLayerById(layerid: number){
    
    return this.http.delete("http://localhost:8080/WEVMapping/rest/delete/layer/" + layerid).subscribe(
      (val) => {
          console.log("DELETE call successful value returned in body", 
                      val);
      },
      response => {
          console.log("DELETE call in error", response);
      },
      () => {
          console.log("The DELETE observable is now completed.");
      });
     
  
  
  }

  deleteFieldById(fieldId:number){
    return this.http.delete("http://localhost:8080/WEVMapping/rest/delete/field/" + fieldId).subscribe(
      (val) => {
          console.log("DELETE call successful value returned in body", 
                      val);
      },
      response => {
          console.log("DELETE call in error", response);
      },
      () => {
          console.log("The DELETE observable is now completed.");
      });
  }

    deleteFeatureById(featureid: number){
    
    return this.http.delete("http://localhost:8080/WEVMapping/rest/delete/feature/" + featureid).subscribe(
      (val) => {
          console.log("DELETE call successful value returned in body", 
                      val);
      },
      response => {
          console.log("DELETE call in error", response);
      },
      () => {
          console.log("The DELETE observable is now completed.");
      });
     
  
  
  }


  addLayer(nombre:string, fields: Array<Field>){
  
    let json = JSON.stringify(fields);
    let url = 'http://localhost:8080/WEVMapping/rest/add/layer/'+nombre;
    let headers = new HttpHeaders().set('Content-Type','application/json');
    
    return this.http.post(url, json, {headers: headers,responseType: 'text'});
  } 
  addFeature(layerID:number, feature: Feature){
    let json = JSON.stringify(feature);
    let url = 'http://localhost:8080/WEVMapping/rest/add/feature/'+layerID;
    let headers = new HttpHeaders().set('Content-Type','application/json');
    
    return this.http.post(url, json, {headers: headers, responseType:'text'});
  }
  registerUser(name:string, email:string, pass:string){
    let p = new Person(email,pass,name);
    let json = JSON.stringify(p);
    let url = 'http://localhost:8080/WEVMapping/rest/add/user/';
    let headers = new HttpHeaders().set('Content-Type','application/json');
    return this.http.post(url, json, {headers: headers, responseType: 'text'}).subscribe((val)=> {
      console.log(val);

    });


  }

  login(userName:string, pass:string){
    let p = new Person('',pass,userName);
    let json = JSON.stringify(p);
    let url = 'http://localhost:8080/WEVMapping/rest/login/';
    let headers = new HttpHeaders().set('Content-Type','application/json');
    let response;
    return this.http.post(url, json, {headers: headers, responseType: 'text'});


  }
  getUserID(userName:string){
    let p = new Person('','pass',userName);
    let json = JSON.stringify(p);
    let url = 'http://localhost:8080/WEVMapping/rest/login/userid';
    let headers = new HttpHeaders().set('Content-Type','application/json');
    let response;
    return this.http.post(url, json, {headers: headers, responseType: 'text'});


  }
  
      private handleError (error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.status);
          }
  


}

export class Person{
  personID: number;
  email: string;
  password: string;
  name: string;
  isManager: boolean;

  constructor(email: string,password: string,name: string){
    this.email = email;
    this.password = password;
    this.name = name;
  }

}
