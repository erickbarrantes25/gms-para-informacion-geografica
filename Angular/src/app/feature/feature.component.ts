import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-feature',
  templateUrl: './feature.component.html',
  styleUrls: ['./feature.component.css']
})
export class FeatureComponent implements OnInit {
  _ref:any;   
  removeObject(){
    this._ref.destroy();
  }  
  save(){
    alert('Saved Successfully!');
    
  }
  
  constructor() { }

  ngOnInit() {
  }

}
