import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { HttpService } from '../http.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private router:Router,private user:UserService,public service:HttpService) { }

  ngOnInit() {
  }

  log(x){
    console.log(x);
  }

  register(e){
     
    e.preventDefault();
    console.log(e);
    var name = e.target.elements[0].value;
    var email = e.target.elements[1].value;
    var password = e.target.elements[2].value;

    this.service.registerUser(name,email,password);
    alert("Registered successful");

    this.router.navigate(['/']);
    
   
  
    
    
  }
}
